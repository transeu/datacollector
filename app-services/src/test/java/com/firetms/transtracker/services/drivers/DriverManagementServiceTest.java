package com.firetms.transtracker.services.drivers;

import com.firetms.transtracker.domain.drivers.Driver;
import com.firetms.transtracker.domain.drivers.DriverRepository;
import info.solidsoft.mockito.java8.api.WithBDDMockito;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static info.solidsoft.mockito.java8.AssertionMatcher.assertArg;
import static org.assertj.core.api.Assertions.assertThat;

public class DriverManagementServiceTest implements WithBDDMockito {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    DriverManagementService service;

    @Mock
    DriverRepository repo;

    @Before
    public void setUp() {
        service = new DriverManagementService( repo );
    }

    @Test
    public void createShouldReplaceEmptyIdWithNull() {
        Driver entity = Driver.aDriver().id( "" ).name( "Gal" ).surname( "Anonim" ).build();
        service.create( entity );

        verify(repo).save( assertArg( (Driver e) ->  assertThat( e.getId() ).isNull() ) );
    }

}