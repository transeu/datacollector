package com.firetms.transtracker.services.drivers;

import com.firetms.transtracker.domain.drivers.Driver;
import com.firetms.transtracker.domain.drivers.DriverRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class DriverManagementService {

    final DriverRepository repo;

    @Autowired
    public DriverManagementService( DriverRepository repo ) {
        this.repo = repo;
    }

    public Driver create( Driver entity ) {
        if (StringUtils.isEmpty( entity.getId() )) {
            entity.setId( null );
        }
        return repo.save( entity );
    }

    public Driver read( String id ) {
        return repo.findOne( id );
    }

    public void delete( String id ) {
        repo.delete( id );
    }

    public List<Driver> find() {
        return Lists.newArrayList( repo.findAll().iterator() );
    }

    public List<Driver> find( String query ) {
        return Lists.newArrayList( repo.findBySurnameStartsWithIgnoreCase( query ).iterator() );
    }

}
