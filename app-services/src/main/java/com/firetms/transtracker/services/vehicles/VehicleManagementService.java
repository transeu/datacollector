package com.firetms.transtracker.services.vehicles;

import com.firetms.transtracker.domain.vehicles.Vehicle;
import com.firetms.transtracker.domain.vehicles.VehicleRepository;
import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Service
@RestController
public class VehicleManagementService {

    @Autowired
    VehicleRepository repo;

    @RequestMapping(value = "/vehicles", method = RequestMethod.POST)
    public void createVehicle(Vehicle vehicle) {
        repo.save(vehicle);
    };

    public Vehicle readVehicle(String vin) {
        return repo.findOne(vin);
    };

    public void deleteVehicle(String vin) {
        repo.delete(vin);
    };

    public List<Vehicle> find() {
        return IteratorUtils.toList(repo.findAll().iterator());
    }

    public List<Vehicle> find(String query) {
        return IteratorUtils.toList(repo.findByPlateNumberStartsWithIgnoreCase(query).iterator());
    }

}
