package com.firetms.vanilla.ui.utils;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class IdGenTest {

    @Test
    public void shouldGenerateId() throws Exception {
        String generatedId = IdGen.genId("group", "id");
        assertThat(generatedId, is("groupId"));


    }
}