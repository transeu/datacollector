package com.firetms.vanilla.ui.utils;


import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.server.Resource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;

public class FluentUI {

    public static FluentButton button(String caption, Button.ClickListener listener) {
        return FluentButton.button( caption, listener );
    }

    public static FluentButton button(String caption, Resource icon) {
        return FluentButton.button( caption, icon );
    }

    public static FluentButton button(Resource icon) {
        return FluentButton.button( icon );
    }

    public static FluentButton button(String caption) {
        return FluentButton.button( caption );
    }

    public static FluentButton button() {
        return FluentButton.button();
    }

    public static FluentGrid grid() {
        return FluentGrid.grid();
    }

    public static FluentGrid grid(final Container.Indexed dataSource) {
        return FluentGrid.grid(dataSource);
    }

    public static FluentGrid grid(final String caption, final Container.Indexed dataSource) {
        return FluentGrid.grid(caption, dataSource);
    }

    public static FluentGrid grid(final String caption) {
        return FluentGrid.grid(caption);
    }
    public static FluentHorizontalLayout horizontalLayout() {
        return FluentHorizontalLayout.horizontalLayout();
    }

    public static FluentHorizontalLayout horizontalLayout(final Component... components) {
        return FluentHorizontalLayout.horizontalLayout(components);
    }

    public static FluentLabel label() {
        return FluentLabel.label();
    }

    public static FluentLabel label(final String caption) {
        return FluentLabel.label(caption);
    }

    public static FluentLabel label(final Property<?> property0) {
        return FluentLabel.label(property0);
    }

    public static FluentLabel label(final String string0, final ContentMode contentMode1) {
        return FluentLabel.label(string0, contentMode1);
    }

    public static FluentLabel label(final Property<?> property0, final ContentMode contentMode1) {
        return FluentLabel.label(property0, contentMode1);
    }

    public static FluentTextField textField() {
        return FluentTextField.textField();
    }

    public static FluentTextField textField(final String string0) {
        return FluentTextField.textField(string0);
    }

    public static FluentTextField textField(final Property<?> property0) {
        return FluentTextField.textField(property0);
    }

    public static FluentTextField textField(final String string0, final Property<?> property1) {
        return FluentTextField.textField(string0, property1);
    }

    public static FluentTextField textField(final String string0, final String string1) {
        return FluentTextField.textField(string0, string1);
    }
    public static FluentVerticalLayout verticalLayout() {
        return FluentVerticalLayout.verticalLayout();
    }

    public static FluentVerticalLayout verticalLayout(final Component... components) {
        return FluentVerticalLayout.verticalLayout(components);
    }

    private FluentUI() {
        // hide
    }
}
