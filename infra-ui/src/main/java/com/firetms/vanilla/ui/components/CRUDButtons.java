package com.firetms.vanilla.ui.components;

import com.firetms.transtracker.ui.I18NProvider;
import com.firetms.vanilla.ui.utils.IdGen;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.spring.i18n.I18N;
import org.vaadin.spring.i18n.support.Translatable;

import java.util.Locale;

public class CRUDButtons extends CssLayout implements Translatable {

    interface Id {
        String SAVE = "save";
        String CANCEL = "cancel";
        String DELETE = "delete";
        String CRUD_BUTTONS = "crudButtons";
    }

    protected Button save;
    protected Button cancel;
    protected Button delete;

    private CRUDButtons(String groupId) {
        if( !(UI.getCurrent() instanceof I18NProvider) )
            throw new IllegalStateException( "UI should implement I18NProvider" );
        setId(IdGen.genId(groupId, Id.CRUD_BUTTONS));
        save = new Button("Save", FontAwesome.SAVE);
        cancel = new Button("Cancel");
        delete = new Button("Delete", FontAwesome.TRASH_O );
        setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        save.setId(IdGen.genId(groupId, Id.SAVE));
        cancel.setId(IdGen.genId(groupId, Id.CANCEL));
        delete.setId(IdGen.genId(groupId, Id.DELETE));

        addComponents(save, cancel, delete);
    }

    public static CRUDButtons CRUDButtons(String groupId) {
        return new CRUDButtons(groupId);
    }

    public CRUDButtons saveListener(Button.ClickListener listener) {
        save.addClickListener(listener);
        return this;
    }

    public CRUDButtons cancelListener(Button.ClickListener listener) {
        cancel.addClickListener(listener);
        return this;
    }

    public CRUDButtons deleteListener(Button.ClickListener listener) {
        delete.addClickListener(listener);
        return this;
    }

    public void focusSave() {
        save.focus();
    }


    @Override
    public void updateMessageStrings( Locale locale ) {
        I18N i18N = ((I18NProvider) UI.getCurrent()).get();
        save.setCaption( i18N.get( "save" ) );
        delete.setCaption( i18N.get( "delete" ) );
        cancel.setCaption( i18N.get( "cancel" ) );
    }
}
