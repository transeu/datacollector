
package com.firetms.vanilla.ui.utils;

import com.vaadin.event.ShortcutListener;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.ErrorMessage;
import com.vaadin.server.Resource;
import com.vaadin.ui.Button;
import com.vaadin.ui.HasComponents;

import javax.annotation.Generated;
import java.util.Locale;

@Generated(value = "de.holisticon.toolbox.generator.FluentApiGenerator", date = "2016-07-06 12:52:00")
public final class FluentButton {

    /**
     * delegate
     * 
     */
    private final Button button;

    /**
     * Hide constructor, use static factory methods.
     * 
     */
    private FluentButton(final Button button) {
        this.button = button;
    }

    /**
     * 
     * @return
     *     the created instance
     */
    public final Button get() {
        return button;
    }

    public static FluentButton button(String arg0, Button.ClickListener arg1) {
        return new FluentButton(new Button(arg0, arg1));
    }

    public static FluentButton button(String arg0, Resource arg1) {
        return new FluentButton(new Button(arg0, arg1));
    }

    public static FluentButton button(Resource arg0) {
        return new FluentButton(new Button(arg0));
    }

    public static FluentButton button(String arg0) {
        return new FluentButton(new Button(arg0));
    }

    public static FluentButton button() {
        return new FluentButton(new Button());
    }

    public FluentButton icon(final Resource arg0) {
        button.setIcon(arg0);
        return this;
    }

    public FluentButton locale(final Locale arg0) {
        button.setLocale(arg0);
        return this;
    }

    public FluentButton sizeFull() {
        button.setSizeFull();
        return this;
    }

    public FluentButton visible(final boolean arg0) {
        button.setVisible(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #visible(true)
     */
    public FluentButton visible() {
        return this.visible(true);
    }

    public FluentButton height(final String arg0) {
        button.setHeight(arg0);
        return this;
    }

    public FluentButton height(final float arg0, final com.vaadin.server.Sizeable.Unit arg1) {
        button.setHeight(arg0, arg1);
        return this;
    }

    public FluentButton addShortcutListeners(ShortcutListener... shortcutListeners) {
        for (ShortcutListener shortcutListener: shortcutListeners) {
            button.addShortcutListener(shortcutListener);
        }
        return this;
    }

    public FluentButton immediate(final boolean arg0) {
        button.setImmediate(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #immediate(true)
     */
    public FluentButton immediate() {
        return this.immediate(true);
    }

    public FluentButton addDetachListeners(com.vaadin.server.ClientConnector.DetachListener... detachListeners) {
        for (com.vaadin.server.ClientConnector.DetachListener detachListener: detachListeners) {
            button.addDetachListener(detachListener);
        }
        return this;
    }

    public FluentButton id(final String arg0) {
        button.setId(arg0);
        return this;
    }

    public FluentButton heightUndefined() {
        button.setHeightUndefined();
        return this;
    }

    public FluentButton widthUndefined() {
        button.setWidthUndefined();
        return this;
    }

    public FluentButton disableOnClick(final boolean arg0) {
        button.setDisableOnClick(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #disableOnClick(true)
     */
    public FluentButton disableOnClick() {
        return this.disableOnClick(true);
    }

    public FluentButton addBlurListeners(com.vaadin.event.FieldEvents.BlurListener... blurListeners) {
        for (com.vaadin.event.FieldEvents.BlurListener blurListener: blurListeners) {
            button.addBlurListener(blurListener);
        }
        return this;
    }

    public FluentButton addAttachListeners(com.vaadin.server.ClientConnector.AttachListener... attachListeners) {
        for (com.vaadin.server.ClientConnector.AttachListener attachListener: attachListeners) {
            button.addAttachListener(attachListener);
        }
        return this;
    }

    public FluentButton componentError(final ErrorMessage arg0) {
        button.setComponentError(arg0);
        return this;
    }

    public FluentButton styleName(final String arg0) {
        button.setStyleName(arg0);
        return this;
    }

    public FluentButton captionAsHtml(final boolean arg0) {
        button.setCaptionAsHtml(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #captionAsHtml(true)
     */
    public FluentButton captionAsHtml() {
        return this.captionAsHtml(true);
    }

    public FluentButton enabled(final boolean arg0) {
        button.setEnabled(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #enabled(true)
     */
    public FluentButton enabled() {
        return this.enabled(true);
    }

    public FluentButton tabIndex(final int arg0) {
        button.setTabIndex(arg0);
        return this;
    }

    public FluentButton readOnly(final boolean arg0) {
        button.setReadOnly(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #readOnly(true)
     */
    public FluentButton readOnly() {
        return this.readOnly(true);
    }

    public FluentButton htmlContentAllowed(final boolean arg0) {
        button.setHtmlContentAllowed(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #htmlContentAllowed(true)
     */
    public FluentButton htmlContentAllowed() {
        return this.htmlContentAllowed(true);
    }

    public FluentButton errorHandler(final ErrorHandler arg0) {
        button.setErrorHandler(arg0);
        return this;
    }

    public FluentButton addFocusListeners(com.vaadin.event.FieldEvents.FocusListener... focusListeners) {
        for (com.vaadin.event.FieldEvents.FocusListener focusListener: focusListeners) {
            button.addFocusListener(focusListener);
        }
        return this;
    }

    public FluentButton iconAlternateText(final String arg0) {
        button.setIconAlternateText(arg0);
        return this;
    }

    public FluentButton caption(final String arg0) {
        button.setCaption(arg0);
        return this;
    }

    public FluentButton icon(final Resource arg0, final String arg1) {
        button.setIcon(arg0, arg1);
        return this;
    }

    public FluentButton clickShortcut(final int arg0, final int[] arg1) {
        button.setClickShortcut(arg0, arg1);
        return this;
    }

    public FluentButton primaryStyleName(final String arg0) {
        button.setPrimaryStyleName(arg0);
        return this;
    }

    public FluentButton sizeUndefined() {
        button.setSizeUndefined();
        return this;
    }

    public FluentButton addStyleNames(String... styleNames) {
        for (String styleName: styleNames) {
            button.addStyleName(styleName);
        }
        return this;
    }

    public FluentButton data(final Object arg0) {
        button.setData(arg0);
        return this;
    }

    public FluentButton width(final String arg0) {
        button.setWidth(arg0);
        return this;
    }

    public FluentButton width(final float arg0, final com.vaadin.server.Sizeable.Unit arg1) {
        button.setWidth(arg0, arg1);
        return this;
    }

    public FluentButton addListeners(com.vaadin.ui.Component.Listener... listeners) {
        for (com.vaadin.ui.Component.Listener listener: listeners) {
            button.addListener(listener);
        }
        return this;
    }

    public FluentButton parent(final HasComponents arg0) {
        button.setParent(arg0);
        return this;
    }

    public FluentButton addClickListeners(Button.ClickListener... clickListeners) {
        for (Button.ClickListener clickListener: clickListeners) {
            button.addClickListener(clickListener);
        }
        return this;
    }

    public FluentButton description(final String arg0) {
        button.setDescription(arg0);
        return this;
    }

}
