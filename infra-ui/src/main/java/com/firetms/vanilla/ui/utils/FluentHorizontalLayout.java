
package com.firetms.vanilla.ui.utils;

import com.vaadin.event.ShortcutListener;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.ErrorMessage;
import com.vaadin.server.Resource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.HasComponents;
import com.vaadin.ui.HorizontalLayout;

import javax.annotation.Generated;
import java.util.Locale;

@Generated(value = "de.holisticon.toolbox.generator.FluentApiGenerator", date = "2016-07-06 12:52:00")
public final class FluentHorizontalLayout {

    /**
     * delegate
     * 
     */
    private final HorizontalLayout horizontalLayout;

    /**
     * Hide constructor, use static factory methods.
     * 
     */
    private FluentHorizontalLayout(final HorizontalLayout horizontalLayout) {
        this.horizontalLayout = horizontalLayout;
    }

    /**
     * 
     * @return
     *     the created instance
     */
    public final HorizontalLayout get() {
        return horizontalLayout;
    }

    public static FluentHorizontalLayout horizontalLayout() {
        return new FluentHorizontalLayout(new HorizontalLayout());
    }

    public static FluentHorizontalLayout horizontalLayout(Component[] arg0) {
        return new FluentHorizontalLayout(new HorizontalLayout(arg0));
    }

    public FluentHorizontalLayout icon(final Resource arg0) {
        horizontalLayout.setIcon(arg0);
        return this;
    }

    public FluentHorizontalLayout locale(final Locale arg0) {
        horizontalLayout.setLocale(arg0);
        return this;
    }

    public FluentHorizontalLayout sizeFull() {
        horizontalLayout.setSizeFull();
        return this;
    }

    public FluentHorizontalLayout width(final float arg0, final com.vaadin.server.Sizeable.Unit arg1) {
        horizontalLayout.setWidth(arg0, arg1);
        return this;
    }

    public FluentHorizontalLayout visible(final boolean arg0) {
        horizontalLayout.setVisible(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #visible(true)
     */
    public FluentHorizontalLayout visible() {
        return this.visible(true);
    }

    public FluentHorizontalLayout height(final String arg0) {
        horizontalLayout.setHeight(arg0);
        return this;
    }

    public FluentHorizontalLayout addShortcutListeners(ShortcutListener... shortcutListeners) {
        for (ShortcutListener shortcutListener: shortcutListeners) {
            horizontalLayout.addShortcutListener(shortcutListener);
        }
        return this;
    }

    public FluentHorizontalLayout immediate(final boolean arg0) {
        horizontalLayout.setImmediate(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #immediate(true)
     */
    public FluentHorizontalLayout immediate() {
        return this.immediate(true);
    }

    public FluentHorizontalLayout addDetachListeners(com.vaadin.server.ClientConnector.DetachListener... detachListeners) {
        for (com.vaadin.server.ClientConnector.DetachListener detachListener: detachListeners) {
            horizontalLayout.addDetachListener(detachListener);
        }
        return this;
    }

    public FluentHorizontalLayout id(final String arg0) {
        horizontalLayout.setId(arg0);
        return this;
    }

    public FluentHorizontalLayout margin(final MarginInfo arg0) {
        horizontalLayout.setMargin(arg0);
        return this;
    }

    public FluentHorizontalLayout margin(final boolean arg0) {
        horizontalLayout.setMargin(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #margin(true)
     */
    public FluentHorizontalLayout margin() {
        return this.margin(true);
    }

    public FluentHorizontalLayout heightUndefined() {
        horizontalLayout.setHeightUndefined();
        return this;
    }

    public FluentHorizontalLayout widthUndefined() {
        horizontalLayout.setWidthUndefined();
        return this;
    }

    public FluentHorizontalLayout addComponents(Component... components) {
        for (Component component: components) {
            horizontalLayout.addComponent(component);
        }
        return this;
    }

    public FluentHorizontalLayout addLayoutClickListeners(com.vaadin.event.LayoutEvents.LayoutClickListener... layoutClickListeners) {
        for (com.vaadin.event.LayoutEvents.LayoutClickListener layoutClickListener: layoutClickListeners) {
            horizontalLayout.addLayoutClickListener(layoutClickListener);
        }
        return this;
    }

    public FluentHorizontalLayout defaultComponentAlignment(final Alignment arg0) {
        horizontalLayout.setDefaultComponentAlignment(arg0);
        return this;
    }

    public FluentHorizontalLayout addAttachListeners(com.vaadin.server.ClientConnector.AttachListener... attachListeners) {
        for (com.vaadin.server.ClientConnector.AttachListener attachListener: attachListeners) {
            horizontalLayout.addAttachListener(attachListener);
        }
        return this;
    }

    public FluentHorizontalLayout addComponentss(Component[]... componentss) {
        for (Component[] components: componentss) {
            horizontalLayout.addComponents(components);
        }
        return this;
    }

    public FluentHorizontalLayout addComponentAttachListeners(HasComponents.ComponentAttachListener... componentAttachListeners) {
        for (HasComponents.ComponentAttachListener componentAttachListener: componentAttachListeners) {
            horizontalLayout.addComponentAttachListener(componentAttachListener);
        }
        return this;
    }

    public FluentHorizontalLayout componentError(final ErrorMessage arg0) {
        horizontalLayout.setComponentError(arg0);
        return this;
    }

    public FluentHorizontalLayout styleName(final String arg0) {
        horizontalLayout.setStyleName(arg0);
        return this;
    }

    public FluentHorizontalLayout captionAsHtml(final boolean arg0) {
        horizontalLayout.setCaptionAsHtml(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #captionAsHtml(true)
     */
    public FluentHorizontalLayout captionAsHtml() {
        return this.captionAsHtml(true);
    }

    public FluentHorizontalLayout enabled(final boolean arg0) {
        horizontalLayout.setEnabled(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #enabled(true)
     */
    public FluentHorizontalLayout enabled() {
        return this.enabled(true);
    }

    public FluentHorizontalLayout readOnly(final boolean arg0) {
        horizontalLayout.setReadOnly(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #readOnly(true)
     */
    public FluentHorizontalLayout readOnly() {
        return this.readOnly(true);
    }

    public FluentHorizontalLayout spacing(final boolean arg0) {
        horizontalLayout.setSpacing(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #spacing(true)
     */
    public FluentHorizontalLayout spacing() {
        return this.spacing(true);
    }

    public FluentHorizontalLayout addComponentAsFirsts(Component... componentAsFirsts) {
        for (Component componentAsFirst: componentAsFirsts) {
            horizontalLayout.addComponentAsFirst(componentAsFirst);
        }
        return this;
    }

    public FluentHorizontalLayout errorHandler(final ErrorHandler arg0) {
        horizontalLayout.setErrorHandler(arg0);
        return this;
    }

    public FluentHorizontalLayout componentAlignment(final Component arg0, final Alignment arg1) {
        horizontalLayout.setComponentAlignment(arg0, arg1);
        return this;
    }

    public FluentHorizontalLayout caption(final String arg0) {
        horizontalLayout.setCaption(arg0);
        return this;
    }

    public FluentHorizontalLayout primaryStyleName(final String arg0) {
        horizontalLayout.setPrimaryStyleName(arg0);
        return this;
    }

    public FluentHorizontalLayout sizeUndefined() {
        horizontalLayout.setSizeUndefined();
        return this;
    }

    public FluentHorizontalLayout height(final float arg0, final com.vaadin.server.Sizeable.Unit arg1) {
        horizontalLayout.setHeight(arg0, arg1);
        return this;
    }

    public FluentHorizontalLayout addStyleNames(String... styleNames) {
        for (String styleName: styleNames) {
            horizontalLayout.addStyleName(styleName);
        }
        return this;
    }

    public FluentHorizontalLayout data(final Object arg0) {
        horizontalLayout.setData(arg0);
        return this;
    }

    public FluentHorizontalLayout width(final String arg0) {
        horizontalLayout.setWidth(arg0);
        return this;
    }

    public FluentHorizontalLayout addListeners(Component.Listener... listeners) {
        for (Component.Listener listener: listeners) {
            horizontalLayout.addListener(listener);
        }
        return this;
    }

    public FluentHorizontalLayout addComponentDetachListeners(HasComponents.ComponentDetachListener... componentDetachListeners) {
        for (HasComponents.ComponentDetachListener componentDetachListener: componentDetachListeners) {
            horizontalLayout.addComponentDetachListener(componentDetachListener);
        }
        return this;
    }

    public FluentHorizontalLayout parent(final HasComponents arg0) {
        horizontalLayout.setParent(arg0);
        return this;
    }

    public FluentHorizontalLayout expandRatio(final Component arg0, final float arg1) {
        horizontalLayout.setExpandRatio(arg0, arg1);
        return this;
    }

    public FluentHorizontalLayout description(final String arg0) {
        horizontalLayout.setDescription(arg0);
        return this;
    }

}
