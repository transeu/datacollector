package com.firetms.vanilla.ui;

import com.firetms.transtracker.ui.I18NProvider;
import com.firetms.vanilla.ui.components.CRUDButtons;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.vaadin.spring.i18n.I18N;
import org.vaadin.spring.i18n.support.Translatable;

import java.util.Locale;

/**
 * Base class for form
 */
public abstract class BaseCRUDForm<T> extends VerticalLayout implements Translatable {
    private static final Logger log = LoggerFactory.getLogger( BaseCRUDForm.class );

    public interface ChangeListener {
        void onChange();
    }

    protected T entity;
    //final protected UI ui;

    /* Action buttons */
    protected CRUDButtons actions;

    public BaseCRUDForm( String formId ) {
        initFields();
        setAdditionalIds();

        actions = FUI.crudButtons( formId );

        // Configure and style components
        setSpacing( true );
        wireButtons();

        setVisible( false );
    }


    protected void initFields() {
    }

    public void setChangeListener( ChangeListener h ) {
        // ChangeHandler is notified when action button is clicked
        actions.saveListener( e -> h.onChange() );
        actions.deleteListener( e -> h.onChange() );
        actions.cancelListener( e -> h.onChange() );
    }

    // wire action buttons to save, delete and reset
    protected void wireButtons() {
    }

    abstract protected void instantiateEntity();

    public void edit( T source ) {
        instantiateEntity();
        BeanUtils.copyProperties( source, entity );

        // Bind customer properties to similarly named fields
        // Could also use annotation or "manual binding" or programmatically
        // moving values from fields to entities before saving
        BeanFieldGroup.bindFieldsUnbuffered( entity, this );
        setVisible( true );
        // A hack to ensure the whole form is visible
        actions.focusSave();

        focusForEditing();
    }

    protected void focusForEditing() {
        //set focus to desired field, e.g.:
//        name.selectAll();
    }

    protected void setAdditionalIds() {
    }

    @Override
    public void setVisible( boolean visible ) {
        super.setVisible( visible );
        actions.setVisible( visible );
    }

    @Override
    public void updateMessageStrings( Locale locale ) {
        I18N i18N = ((I18NProvider) UI.getCurrent()).get();
        updateMessageStrings( i18N, locale );
    }

    protected abstract void updateMessageStrings( I18N i18N,  Locale locale );
}
