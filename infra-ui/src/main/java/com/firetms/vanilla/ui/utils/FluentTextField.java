
package com.firetms.vanilla.ui.utils;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ShortcutListener;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.ErrorMessage;
import com.vaadin.server.Resource;
import com.vaadin.ui.HasComponents;
import com.vaadin.ui.TextField;

import javax.annotation.Generated;
import java.util.Locale;

public final class FluentTextField {

    /**
     * delegate
     * 
     */
    private final TextField textField;

    /**
     * Hide constructor, use static factory methods.
     * 
     */
    private FluentTextField(final TextField textField) {
        this.textField = textField;
    }

    /**
     * 
     * @return
     *     the created instance
     */
    public final TextField get() {
        return textField;
    }


    public static FluentTextField textField(String caption, String value) {
        return new FluentTextField(new TextField(caption, value));
    }

    public static FluentTextField textField(String caption, Property property) {
        return new FluentTextField(new TextField(caption, property));
    }

    public static FluentTextField textField(Property property) {
        return new FluentTextField(new TextField(property));
    }

    public static FluentTextField textField(String caption) {
        return new FluentTextField(new TextField(caption));
    }

    public static FluentTextField textField() {
        return new FluentTextField(new TextField());
    }

    public FluentTextField converter(final Converter arg0) {
        textField.setConverter(arg0);
        return this;
    }

    public FluentTextField converter(final Class arg0) {
        textField.setConverter(arg0);
        return this;
    }

    public FluentTextField icon(final Resource arg0) {
        textField.setIcon(arg0);
        return this;
    }

    public FluentTextField locale(final Locale arg0) {
        textField.setLocale(arg0);
        return this;
    }

    public FluentTextField sizeFull() {
        textField.setSizeFull();
        return this;
    }

    public FluentTextField buffered(final boolean arg0) {
        textField.setBuffered(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #buffered(true)
     */
    public FluentTextField buffered() {
        return this.buffered(true);
    }

    public FluentTextField addBlurListeners(com.vaadin.event.FieldEvents.BlurListener... blurListeners) {
        for (com.vaadin.event.FieldEvents.BlurListener blurListener: blurListeners) {
            textField.addBlurListener(blurListener);
        }
        return this;
    }

    public FluentTextField visible(final boolean arg0) {
        textField.setVisible(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #visible(true)
     */
    public FluentTextField visible() {
        return this.visible(true);
    }

    public FluentTextField currentBufferedSourceException(final com.vaadin.data.Buffered.SourceException arg0) {
        textField.setCurrentBufferedSourceException(arg0);
        return this;
    }

    public FluentTextField height(final String arg0) {
        textField.setHeight(arg0);
        return this;
    }

    public FluentTextField height(final float arg0, final com.vaadin.server.Sizeable.Unit arg1) {
        textField.setHeight(arg0, arg1);
        return this;
    }

    public FluentTextField addShortcutListeners(ShortcutListener... shortcutListeners) {
        for (ShortcutListener shortcutListener: shortcutListeners) {
            textField.addShortcutListener(shortcutListener);
        }
        return this;
    }

    public FluentTextField immediate(final boolean arg0) {
        textField.setImmediate(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #immediate(true)
     */
    public FluentTextField immediate() {
        return this.immediate(true);
    }

    public FluentTextField addDetachListeners(com.vaadin.server.ClientConnector.DetachListener... detachListeners) {
        for (com.vaadin.server.ClientConnector.DetachListener detachListener: detachListeners) {
            textField.addDetachListener(detachListener);
        }
        return this;
    }

    public FluentTextField nullSettingAllowed(final boolean arg0) {
        textField.setNullSettingAllowed(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #nullSettingAllowed(true)
     */
    public FluentTextField nullSettingAllowed() {
        return this.nullSettingAllowed(true);
    }

    public FluentTextField id(final String arg0) {
        textField.setId(arg0);
        return this;
    }

    public FluentTextField selectionRange(final int arg0, final int arg1) {
        textField.setSelectionRange(arg0, arg1);
        return this;
    }

    public FluentTextField requiredError(final String arg0) {
        textField.setRequiredError(arg0);
        return this;
    }

    public FluentTextField nullRepresentation(final String arg0) {
        textField.setNullRepresentation(arg0);
        return this;
    }

    public FluentTextField invalidAllowed(final boolean arg0) {
        textField.setInvalidAllowed(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #invalidAllowed(true)
     */
    public FluentTextField invalidAllowed() {
        return this.invalidAllowed(true);
    }

    public FluentTextField tabIndex(final int arg0) {
        textField.setTabIndex(arg0);
        return this;
    }

    public FluentTextField addReadOnlyStatusChangeListeners(Property.ReadOnlyStatusChangeListener... readOnlyStatusChangeListeners) {
        for (Property.ReadOnlyStatusChangeListener readOnlyStatusChangeListener: readOnlyStatusChangeListeners) {
            textField.addReadOnlyStatusChangeListener(readOnlyStatusChangeListener);
        }
        return this;
    }

    public FluentTextField heightUndefined() {
        textField.setHeightUndefined();
        return this;
    }

    public FluentTextField widthUndefined() {
        textField.setWidthUndefined();
        return this;
    }

    public FluentTextField addTextChangeListeners(com.vaadin.event.FieldEvents.TextChangeListener... textChangeListeners) {
        for (com.vaadin.event.FieldEvents.TextChangeListener textChangeListener: textChangeListeners) {
            textField.addTextChangeListener(textChangeListener);
        }
        return this;
    }

    public FluentTextField textChangeEventMode(final com.vaadin.ui.AbstractTextField.TextChangeEventMode arg0) {
        textField.setTextChangeEventMode(arg0);
        return this;
    }

    public FluentTextField validationVisible(final boolean arg0) {
        textField.setValidationVisible(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #validationVisible(true)
     */
    public FluentTextField validationVisible() {
        return this.validationVisible(true);
    }

    public FluentTextField cursorPosition(final int arg0) {
        textField.setCursorPosition(arg0);
        return this;
    }

    public FluentTextField addAttachListeners(com.vaadin.server.ClientConnector.AttachListener... attachListeners) {
        for (com.vaadin.server.ClientConnector.AttachListener attachListener: attachListeners) {
            textField.addAttachListener(attachListener);
        }
        return this;
    }

    public FluentTextField convertedValue(final Object arg0) {
        textField.setConvertedValue(arg0);
        return this;
    }

    public FluentTextField componentError(final ErrorMessage arg0) {
        textField.setComponentError(arg0);
        return this;
    }

    public FluentTextField styleName(final String arg0) {
        textField.setStyleName(arg0);
        return this;
    }

    public FluentTextField captionAsHtml(final boolean arg0) {
        textField.setCaptionAsHtml(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #captionAsHtml(true)
     */
    public FluentTextField captionAsHtml() {
        return this.captionAsHtml(true);
    }

    public FluentTextField enabled(final boolean arg0) {
        textField.setEnabled(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #enabled(true)
     */
    public FluentTextField enabled() {
        return this.enabled(true);
    }

    public FluentTextField readOnly(final boolean arg0) {
        textField.setReadOnly(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #readOnly(true)
     */
    public FluentTextField readOnly() {
        return this.readOnly(true);
    }

    public FluentTextField maxLength(final int arg0) {
        textField.setMaxLength(arg0);
        return this;
    }

    public FluentTextField textChangeTimeout(final int arg0) {
        textField.setTextChangeTimeout(arg0);
        return this;
    }

    public FluentTextField conversionError(final String arg0) {
        textField.setConversionError(arg0);
        return this;
    }

    public FluentTextField invalidCommitted(final boolean arg0) {
        textField.setInvalidCommitted(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #invalidCommitted(true)
     */
    public FluentTextField invalidCommitted() {
        return this.invalidCommitted(true);
    }

    public FluentTextField addValidators(Validator... validators) {
        for (Validator validator: validators) {
            textField.addValidator(validator);
        }
        return this;
    }

    public FluentTextField errorHandler(final ErrorHandler arg0) {
        textField.setErrorHandler(arg0);
        return this;
    }

    public FluentTextField required(final boolean arg0) {
        textField.setRequired(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #required(true)
     */
    public FluentTextField required() {
        return this.required(true);
    }

    public FluentTextField caption(final String arg0) {
        textField.setCaption(arg0);
        return this;
    }

    public FluentTextField value(final String arg0) {
        textField.setValue(arg0);
        return this;
    }

    public FluentTextField addValueChangeListeners(Property.ValueChangeListener... valueChangeListeners) {
        for (Property.ValueChangeListener valueChangeListener: valueChangeListeners) {
            textField.addValueChangeListener(valueChangeListener);
        }
        return this;
    }

    public FluentTextField primaryStyleName(final String arg0) {
        textField.setPrimaryStyleName(arg0);
        return this;
    }

    public FluentTextField sizeUndefined() {
        textField.setSizeUndefined();
        return this;
    }

    public FluentTextField addStyleNames(String... styleNames) {
        for (String styleName: styleNames) {
            textField.addStyleName(styleName);
        }
        return this;
    }

    public FluentTextField data(final Object arg0) {
        textField.setData(arg0);
        return this;
    }

    public FluentTextField width(final String arg0) {
        textField.setWidth(arg0);
        return this;
    }

    public FluentTextField width(final float arg0, final com.vaadin.server.Sizeable.Unit arg1) {
        textField.setWidth(arg0, arg1);
        return this;
    }

    public FluentTextField addListeners(com.vaadin.ui.Component.Listener... listeners) {
        for (com.vaadin.ui.Component.Listener listener: listeners) {
            textField.addListener(listener);
        }
        return this;
    }

    public FluentTextField parent(final HasComponents arg0) {
        textField.setParent(arg0);
        return this;
    }

    public FluentTextField columns(final int arg0) {
        textField.setColumns(arg0);
        return this;
    }

    public FluentTextField propertyDataSource(final Property arg0) {
        textField.setPropertyDataSource(arg0);
        return this;
    }

    public FluentTextField addFocusListeners(com.vaadin.event.FieldEvents.FocusListener... focusListeners) {
        for (com.vaadin.event.FieldEvents.FocusListener focusListener: focusListeners) {
            textField.addFocusListener(focusListener);
        }
        return this;
    }

    public FluentTextField inputPrompt(final String arg0) {
        textField.setInputPrompt(arg0);
        return this;
    }

    public FluentTextField description(final String arg0) {
        textField.setDescription(arg0);
        return this;
    }

}
