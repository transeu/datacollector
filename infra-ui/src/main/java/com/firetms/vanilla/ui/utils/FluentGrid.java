
package com.firetms.vanilla.ui.utils;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroupFieldFactory;
import com.vaadin.event.ShortcutListener;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.ErrorMessage;
import com.vaadin.server.Resource;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HasComponents;

import javax.annotation.Generated;
import java.util.List;
import java.util.Locale;

@Generated(value = "de.holisticon.toolbox.generator.FluentApiGenerator", date = "2016-07-06 12:52:00")
public final class FluentGrid {

    /**
     * delegate
     * 
     */
    private final Grid grid;

    /**
     * Hide constructor, use static factory methods.
     * 
     */
    private FluentGrid(final Grid grid) {
        this.grid = grid;
    }

    /**
     * 
     * @return
     *     the created instance
     */
    public final Grid get() {
        return grid;
    }

    public static FluentGrid grid(String arg0, com.vaadin.data.Container.Indexed arg1) {
        return new FluentGrid(new Grid(arg0, arg1));
    }

    public static FluentGrid grid(String arg0) {
        return new FluentGrid(new Grid(arg0));
    }

    public static FluentGrid grid(com.vaadin.data.Container.Indexed arg0) {
        return new FluentGrid(new Grid(arg0));
    }

    public static FluentGrid grid() {
        return new FluentGrid(new Grid());
    }

    public FluentGrid icon(final Resource arg0) {
        grid.setIcon(arg0);
        return this;
    }

    public FluentGrid cellStyleGenerator(final Grid.CellStyleGenerator arg0) {
        grid.setCellStyleGenerator(arg0);
        return this;
    }

    public FluentGrid locale(final Locale arg0) {
        grid.setLocale(arg0);
        return this;
    }

    public FluentGrid sizeFull() {
        grid.setSizeFull();
        return this;
    }

    public FluentGrid visible(final boolean arg0) {
        grid.setVisible(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #visible(true)
     */
    public FluentGrid visible() {
        return this.visible(true);
    }

    public FluentGrid height(final String arg0) {
        grid.setHeight(arg0);
        return this;
    }

    public FluentGrid addSelectionListeners(com.vaadin.event.SelectionEvent.SelectionListener... selectionListeners) {
        for (com.vaadin.event.SelectionEvent.SelectionListener selectionListener: selectionListeners) {
            grid.addSelectionListener(selectionListener);
        }
        return this;
    }

    public FluentGrid addShortcutListeners(ShortcutListener... shortcutListeners) {
        for (ShortcutListener shortcutListener: shortcutListeners) {
            grid.addShortcutListener(shortcutListener);
        }
        return this;
    }

    public FluentGrid immediate(final boolean arg0) {
        grid.setImmediate(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #immediate(true)
     */
    public FluentGrid immediate() {
        return this.immediate(true);
    }

    public FluentGrid addDetachListeners(com.vaadin.server.ClientConnector.DetachListener... detachListeners) {
        for (com.vaadin.server.ClientConnector.DetachListener detachListener: detachListeners) {
            grid.addDetachListener(detachListener);
        }
        return this;
    }

    public FluentGrid defaultHeaderRow(final Grid.HeaderRow arg0) {
        grid.setDefaultHeaderRow(arg0);
        return this;
    }

    public FluentGrid heightMode(final HeightMode arg0) {
        grid.setHeightMode(arg0);
        return this;
    }

    public FluentGrid editorFieldGroup(final FieldGroup arg0) {
        grid.setEditorFieldGroup(arg0);
        return this;
    }

    public FluentGrid id(final String arg0) {
        grid.setId(arg0);
        return this;
    }

    public FluentGrid columnOrder(final Object[] arg0) {
        grid.setColumnOrder(arg0);
        return this;
    }

    public FluentGrid editorEnabled(final boolean arg0) {
        grid.setEditorEnabled(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #editorEnabled(true)
     */
    public FluentGrid editorEnabled() {
        return this.editorEnabled(true);
    }

    public FluentGrid editorFieldFactory(final FieldGroupFieldFactory arg0) {
        grid.setEditorFieldFactory(arg0);
        return this;
    }

    public FluentGrid heightUndefined() {
        grid.setHeightUndefined();
        return this;
    }

    public FluentGrid addSortListeners(com.vaadin.event.SortEvent.SortListener... sortListeners) {
        for (com.vaadin.event.SortEvent.SortListener sortListener: sortListeners) {
            grid.addSortListener(sortListener);
        }
        return this;
    }

    public FluentGrid widthUndefined() {
        grid.setWidthUndefined();
        return this;
    }

    public FluentGrid selectionModel(final Grid.SelectionModel arg0) {
        grid.setSelectionModel(arg0);
        return this;
    }

    public FluentGrid selectionMode(final Grid.SelectionMode selectionMode) {
        grid.setSelectionMode(selectionMode);
        return this;
    }

    public FluentGrid frozenColumnCount(final int arg0) {
        grid.setFrozenColumnCount(arg0);
        return this;
    }

    public FluentGrid footerVisible(final boolean arg0) {
        grid.setFooterVisible(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #footerVisible(true)
     */
    public FluentGrid footerVisible() {
        return this.footerVisible(true);
    }

    public FluentGrid sortOrder(final List arg0) {
        grid.setSortOrder(arg0);
        return this;
    }

    public FluentGrid editorErrorHandler(final Grid.EditorErrorHandler arg0) {
        grid.setEditorErrorHandler(arg0);
        return this;
    }

    public FluentGrid editorCancelCaption(final String arg0) {
        grid.setEditorCancelCaption(arg0);
        return this;
    }

    public FluentGrid addAttachListeners(com.vaadin.server.ClientConnector.AttachListener... attachListeners) {
        for (com.vaadin.server.ClientConnector.AttachListener attachListener: attachListeners) {
            grid.addAttachListener(attachListener);
        }
        return this;
    }

    public FluentGrid editorSaveCaption(final String arg0) {
        grid.setEditorSaveCaption(arg0);
        return this;
    }

    public FluentGrid componentError(final ErrorMessage arg0) {
        grid.setComponentError(arg0);
        return this;
    }

    public FluentGrid styleName(final String arg0) {
        grid.setStyleName(arg0);
        return this;
    }

    public FluentGrid captionAsHtml(final boolean arg0) {
        grid.setCaptionAsHtml(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #captionAsHtml(true)
     */
    public FluentGrid captionAsHtml() {
        return this.captionAsHtml(true);
    }

    public FluentGrid enabled(final boolean arg0) {
        grid.setEnabled(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #enabled(true)
     */
    public FluentGrid enabled() {
        return this.enabled(true);
    }

    public FluentGrid readOnly(final boolean arg0) {
        grid.setReadOnly(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #readOnly(true)
     */
    public FluentGrid readOnly() {
        return this.readOnly(true);
    }

    public FluentGrid addItemClickListeners(com.vaadin.event.ItemClickEvent.ItemClickListener... itemClickListeners) {
        for (com.vaadin.event.ItemClickEvent.ItemClickListener itemClickListener: itemClickListeners) {
            grid.addItemClickListener(itemClickListener);
        }
        return this;
    }

    public FluentGrid heightByRows(final double arg0) {
        grid.setHeightByRows(arg0);
        return this;
    }

    public FluentGrid errorHandler(final ErrorHandler arg0) {
        grid.setErrorHandler(arg0);
        return this;
    }

    public FluentGrid headerVisible(final boolean arg0) {
        grid.setHeaderVisible(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #headerVisible(true)
     */
    public FluentGrid headerVisible() {
        return this.headerVisible(true);
    }

    public FluentGrid caption(final String arg0) {
        grid.setCaption(arg0);
        return this;
    }

    public FluentGrid rowStyleGenerator(final Grid.RowStyleGenerator arg0) {
        grid.setRowStyleGenerator(arg0);
        return this;
    }

    public FluentGrid primaryStyleName(final String arg0) {
        grid.setPrimaryStyleName(arg0);
        return this;
    }

    public FluentGrid sizeUndefined() {
        grid.setSizeUndefined();
        return this;
    }

    public FluentGrid addStyleNames(String... styleNames) {
        for (String styleName: styleNames) {
            grid.addStyleName(styleName);
        }
        return this;
    }

    public FluentGrid data(final Object arg0) {
        grid.setData(arg0);
        return this;
    }

    public FluentGrid height(final float arg0, final com.vaadin.server.Sizeable.Unit arg1) {
        grid.setHeight(arg0, arg1);
        return this;
    }

    public FluentGrid containerDataSource(final com.vaadin.data.Container.Indexed arg0) {
        grid.setContainerDataSource(arg0);
        return this;
    }

    public FluentGrid width(final String arg0) {
        grid.setWidth(arg0);
        return this;
    }

    public FluentGrid width(final float arg0, final com.vaadin.server.Sizeable.Unit arg1) {
        grid.setWidth(arg0, arg1);
        return this;
    }

    public FluentGrid addListeners(com.vaadin.ui.Component.Listener... listeners) {
        for (com.vaadin.ui.Component.Listener listener: listeners) {
            grid.addListener(listener);
        }
        return this;
    }

    public FluentGrid parent(final HasComponents arg0) {
        grid.setParent(arg0);
        return this;
    }

    public FluentGrid description(final String arg0) {
        grid.setDescription(arg0);
        return this;
    }

}
