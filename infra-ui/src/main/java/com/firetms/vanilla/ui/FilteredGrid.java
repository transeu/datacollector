package com.firetms.vanilla.ui;

import com.firetms.transtracker.ui.I18NProvider;
import com.firetms.vanilla.ui.utils.FluentUI;
import com.vaadin.data.Container;
import com.vaadin.event.FieldEvents;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.spring.i18n.I18N;
import org.vaadin.spring.i18n.support.Translatable;

import java.util.Collection;
import java.util.Locale;

/**
 * Grid component with filter input field and button
 */
public class FilteredGrid extends VerticalLayout implements Translatable {
    private static final Logger log = LoggerFactory.getLogger( FilteredGrid.class );
    private String prefix;
    private HorizontalLayout actions;

    public interface Id {
        String GRID = "_grid";
        String FILTER = "_filter";
        String BUTTON = "_button";
    }

    public interface SelectionListener<T> {
        void select( T item );
    }

    public interface FilterTriggeredListener {
        void trigger( String query );
    }

    private final Grid grid;
    private final TextField filter;
    private final Button button;

    public FilteredGrid( String id, String filterPromptKey ) {
        setId( id );
        grid = FluentUI.grid().id( id + Id.GRID )
                       .selectionMode( Grid.SelectionMode.SINGLE )
                       .get();
        filter = FluentUI.textField()
                         .id( id + Id.FILTER )
                         .data( filterPromptKey )
                         .get();
        button = FluentUI.button( FontAwesome.FILTER )
                         .id( id + Id.BUTTON )
                         .get();
        actions = FluentUI.horizontalLayout( filter, button )/*.spacing()*/.get();
        addComponents( actions, grid);
    }

    public FilteredGrid addAction( Component component ) {
        actions.addComponent( component );
        return this;
    }

    public FilteredGrid heightByRows( double rows ) {
        grid.setHeightByRows( rows );
        return this;
    }

    public FilteredGrid columnOrder( Object... propertyIds ) {
        grid.setColumnOrder( propertyIds );
        return this;
    }

    public FilteredGrid removeColumn( Object propertyId ) {
        grid.removeColumn( propertyId );
        return this;
    }

    public FilteredGrid translationPrefix( String prefix ) {
        this.prefix = prefix;
        return this;
    }

    private void translateColumns( I18N i18N, Locale locale ) {
        Collection<?> ids = grid.getContainerDataSource().getContainerPropertyIds();
        for (Object o : ids) {
            String col = (String) o;
            String name = prefix != null ? String.format( "%s.%s", prefix, col ) : col;
            Grid.Column column = grid.getColumn( col );
            if(column != null )
                column.setHeaderCaption( i18N.get( name, locale ) );
        }
    }

    public <T> FilteredGrid addSelectionListener( SelectionListener<T> listener ) {
        grid.addSelectionListener( e -> {
            // Connect selected driver to editor or hide if none is selected
            if (e.getSelected().isEmpty()) {
                listener.select( null );
            } else {
                //noinspection unchecked
                listener.select( (T) grid.getSelectedRow() );
            }
        } );
        return this;
    }

    public FilteredGrid addFilterChangeListener( FieldEvents.TextChangeListener listener ) {
        filter.addTextChangeListener( listener );
        return this;
    }

    public FilteredGrid addFilterTriggerListener( FilterTriggeredListener listener ) {
        button.addClickListener( e -> listener.trigger( filter.getValue() ) );
        return this;
    }

    public FilteredGrid id( String id ) {
        setId( id );
        return this;
    }

    /**
     * Returns current filter
     */
    public String getFilter() {
        return filter.getValue();
    }

    public FilteredGrid dataSource(Container.Indexed container) {
        grid.setContainerDataSource( container );
        return this;
    }

    public FilteredGrid gridHeight(float height, Unit unit) {
        grid.setHeight( height, unit );
        return this;
    }

    @Override
    public void updateMessageStrings( Locale locale ) {
        I18N i18N = ((I18NProvider) UI.getCurrent()).get();
        button.setCaption( i18N.get( "grid.filter" ) );
        filter.setInputPrompt( i18N.get( (String) filter.getData(), locale ) );
        translateColumns( i18N, locale );
    }

}
