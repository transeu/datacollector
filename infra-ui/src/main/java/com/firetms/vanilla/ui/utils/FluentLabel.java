
package com.firetms.vanilla.ui.utils;

import com.vaadin.data.Property;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ShortcutListener;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.ErrorMessage;
import com.vaadin.server.Resource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.HasComponents;
import com.vaadin.ui.Label;

import javax.annotation.Generated;
import java.util.Locale;

@Generated(value = "de.holisticon.toolbox.generator.FluentApiGenerator", date = "2016-07-06 12:52:00")
public final class FluentLabel {

    /**
     * delegate
     * 
     */
    private final Label label;

    /**
     * Hide constructor, use static factory methods.
     * 
     */
    private FluentLabel(final Label label) {
        this.label = label;
    }

    /**
     * 
     * @return
     *     the created instance
     */
    public final Label get() {
        return label;
    }

    public static FluentLabel label(Property arg0, ContentMode arg1) {
        return new FluentLabel(new Label(arg0, arg1));
    }

    public static FluentLabel label(String arg0, ContentMode arg1) {
        return new FluentLabel(new Label(arg0, arg1));
    }

    public static FluentLabel label(Property arg0) {
        return new FluentLabel(new Label(arg0));
    }

    public static FluentLabel label(String arg0) {
        return new FluentLabel(new Label(arg0));
    }

    public static FluentLabel label() {
        return new FluentLabel(new Label());
    }

    public FluentLabel icon(final Resource arg0) {
        label.setIcon(arg0);
        return this;
    }

    public FluentLabel sizeFull() {
        label.setSizeFull();
        return this;
    }

    public FluentLabel visible(final boolean arg0) {
        label.setVisible(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #visible(true)
     */
    public FluentLabel visible() {
        return this.visible(true);
    }

    public FluentLabel height(final String arg0) {
        label.setHeight(arg0);
        return this;
    }

    public FluentLabel height(final float arg0, final com.vaadin.server.Sizeable.Unit arg1) {
        label.setHeight(arg0, arg1);
        return this;
    }

    public FluentLabel addShortcutListeners(ShortcutListener... shortcutListeners) {
        for (ShortcutListener shortcutListener: shortcutListeners) {
            label.addShortcutListener(shortcutListener);
        }
        return this;
    }

    public FluentLabel immediate(final boolean arg0) {
        label.setImmediate(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #immediate(true)
     */
    public FluentLabel immediate() {
        return this.immediate(true);
    }

    public FluentLabel value(final String arg0) {
        label.setValue(arg0);
        return this;
    }

    public FluentLabel addDetachListeners(com.vaadin.server.ClientConnector.DetachListener... detachListeners) {
        for (com.vaadin.server.ClientConnector.DetachListener detachListener: detachListeners) {
            label.addDetachListener(detachListener);
        }
        return this;
    }

    public FluentLabel contentMode(final ContentMode arg0) {
        label.setContentMode(arg0);
        return this;
    }

    public FluentLabel id(final String arg0) {
        label.setId(arg0);
        return this;
    }

    public FluentLabel heightUndefined() {
        label.setHeightUndefined();
        return this;
    }

    public FluentLabel widthUndefined() {
        label.setWidthUndefined();
        return this;
    }

    public FluentLabel converter(final Converter arg0) {
        label.setConverter(arg0);
        return this;
    }

    public FluentLabel addAttachListeners(com.vaadin.server.ClientConnector.AttachListener... attachListeners) {
        for (com.vaadin.server.ClientConnector.AttachListener attachListener: attachListeners) {
            label.addAttachListener(attachListener);
        }
        return this;
    }

    public FluentLabel locale(final Locale arg0) {
        label.setLocale(arg0);
        return this;
    }

    public FluentLabel componentError(final ErrorMessage arg0) {
        label.setComponentError(arg0);
        return this;
    }

    public FluentLabel styleName(final String arg0) {
        label.setStyleName(arg0);
        return this;
    }

    public FluentLabel captionAsHtml(final boolean arg0) {
        label.setCaptionAsHtml(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #captionAsHtml(true)
     */
    public FluentLabel captionAsHtml() {
        return this.captionAsHtml(true);
    }

    public FluentLabel enabled(final boolean arg0) {
        label.setEnabled(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #enabled(true)
     */
    public FluentLabel enabled() {
        return this.enabled(true);
    }

    public FluentLabel readOnly(final boolean arg0) {
        label.setReadOnly(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #readOnly(true)
     */
    public FluentLabel readOnly() {
        return this.readOnly(true);
    }

    public FluentLabel propertyDataSource(final Property arg0) {
        label.setPropertyDataSource(arg0);
        return this;
    }

    public FluentLabel errorHandler(final ErrorHandler arg0) {
        label.setErrorHandler(arg0);
        return this;
    }

    public FluentLabel addValueChangeListeners(Property.ValueChangeListener... valueChangeListeners) {
        for (Property.ValueChangeListener valueChangeListener: valueChangeListeners) {
            label.addValueChangeListener(valueChangeListener);
        }
        return this;
    }

    public FluentLabel caption(final String arg0) {
        label.setCaption(arg0);
        return this;
    }

    public FluentLabel primaryStyleName(final String arg0) {
        label.setPrimaryStyleName(arg0);
        return this;
    }

    public FluentLabel sizeUndefined() {
        label.setSizeUndefined();
        return this;
    }

    public FluentLabel addStyleNames(String... styleNames) {
        for (String styleName: styleNames) {
            label.addStyleName(styleName);
        }
        return this;
    }

    public FluentLabel data(final Object arg0) {
        label.setData(arg0);
        return this;
    }

    public FluentLabel width(final String arg0) {
        label.setWidth(arg0);
        return this;
    }

    public FluentLabel width(final float arg0, final com.vaadin.server.Sizeable.Unit arg1) {
        label.setWidth(arg0, arg1);
        return this;
    }

    public FluentLabel addListeners(com.vaadin.ui.Component.Listener... listeners) {
        for (com.vaadin.ui.Component.Listener listener: listeners) {
            label.addListener(listener);
        }
        return this;
    }

    public FluentLabel parent(final HasComponents arg0) {
        label.setParent(arg0);
        return this;
    }

    public FluentLabel description(final String arg0) {
        label.setDescription(arg0);
        return this;
    }

}
