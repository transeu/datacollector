package com.firetms.vanilla.ui;

import com.firetms.vanilla.ui.components.CRUDButtons;
import com.firetms.vanilla.ui.elements.FTextField;

public class FUI {

    public static FTextField textField(String id) {
        return FTextField.textField(id);
    }

    public static FTextField textField(String groupId, String id) {
        return FTextField.textField(groupId, id);
    }

    public static CRUDButtons crudButtons(String groupId) {
        return CRUDButtons.CRUDButtons(groupId);
    }
}
