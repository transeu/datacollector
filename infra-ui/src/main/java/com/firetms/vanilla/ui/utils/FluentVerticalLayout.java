
package com.firetms.vanilla.ui.utils;

import com.vaadin.event.ShortcutListener;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.ErrorMessage;
import com.vaadin.server.Resource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.HasComponents;
import com.vaadin.ui.VerticalLayout;

import javax.annotation.Generated;
import java.util.Locale;

@Generated(value = "de.holisticon.toolbox.generator.FluentApiGenerator", date = "2016-07-06 12:52:00")
public final class FluentVerticalLayout {

    /**
     * delegate
     * 
     */
    private final VerticalLayout verticalLayout;

    /**
     * Hide constructor, use static factory methods.
     * 
     */
    private FluentVerticalLayout(final VerticalLayout verticalLayout) {
        this.verticalLayout = verticalLayout;
    }

    /**
     * 
     * @return
     *     the created instance
     */
    public final VerticalLayout get() {
        return verticalLayout;
    }

    public static FluentVerticalLayout verticalLayout() {
        return new FluentVerticalLayout(new VerticalLayout());
    }

    public static FluentVerticalLayout verticalLayout(Component[] arg0) {
        return new FluentVerticalLayout(new VerticalLayout(arg0));
    }

    public FluentVerticalLayout icon(final Resource arg0) {
        verticalLayout.setIcon(arg0);
        return this;
    }

    public FluentVerticalLayout locale(final Locale arg0) {
        verticalLayout.setLocale(arg0);
        return this;
    }

    public FluentVerticalLayout sizeFull() {
        verticalLayout.setSizeFull();
        return this;
    }

    public FluentVerticalLayout width(final float arg0, final com.vaadin.server.Sizeable.Unit arg1) {
        verticalLayout.setWidth(arg0, arg1);
        return this;
    }

    public FluentVerticalLayout visible(final boolean arg0) {
        verticalLayout.setVisible(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #visible(true)
     */
    public FluentVerticalLayout visible() {
        return this.visible(true);
    }

    public FluentVerticalLayout height(final String arg0) {
        verticalLayout.setHeight(arg0);
        return this;
    }

    public FluentVerticalLayout addShortcutListeners(ShortcutListener... shortcutListeners) {
        for (ShortcutListener shortcutListener: shortcutListeners) {
            verticalLayout.addShortcutListener(shortcutListener);
        }
        return this;
    }

    public FluentVerticalLayout immediate(final boolean arg0) {
        verticalLayout.setImmediate(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #immediate(true)
     */
    public FluentVerticalLayout immediate() {
        return this.immediate(true);
    }

    public FluentVerticalLayout addDetachListeners(com.vaadin.server.ClientConnector.DetachListener... detachListeners) {
        for (com.vaadin.server.ClientConnector.DetachListener detachListener: detachListeners) {
            verticalLayout.addDetachListener(detachListener);
        }
        return this;
    }

    public FluentVerticalLayout id(final String arg0) {
        verticalLayout.setId(arg0);
        return this;
    }

    public FluentVerticalLayout margin(final MarginInfo arg0) {
        verticalLayout.setMargin(arg0);
        return this;
    }

    public FluentVerticalLayout margin(final boolean arg0) {
        verticalLayout.setMargin(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #margin(true)
     */
    public FluentVerticalLayout margin() {
        return this.margin(true);
    }

    public FluentVerticalLayout heightUndefined() {
        verticalLayout.setHeightUndefined();
        return this;
    }

    public FluentVerticalLayout widthUndefined() {
        verticalLayout.setWidthUndefined();
        return this;
    }

    public FluentVerticalLayout addComponents(Component... components) {
        for (Component component: components) {
            verticalLayout.addComponent(component);
        }
        return this;
    }

    public FluentVerticalLayout addLayoutClickListeners(com.vaadin.event.LayoutEvents.LayoutClickListener... layoutClickListeners) {
        for (com.vaadin.event.LayoutEvents.LayoutClickListener layoutClickListener: layoutClickListeners) {
            verticalLayout.addLayoutClickListener(layoutClickListener);
        }
        return this;
    }

    public FluentVerticalLayout defaultComponentAlignment(final Alignment arg0) {
        verticalLayout.setDefaultComponentAlignment(arg0);
        return this;
    }

    public FluentVerticalLayout addAttachListeners(com.vaadin.server.ClientConnector.AttachListener... attachListeners) {
        for (com.vaadin.server.ClientConnector.AttachListener attachListener: attachListeners) {
            verticalLayout.addAttachListener(attachListener);
        }
        return this;
    }

    public FluentVerticalLayout addComponentss(Component[]... componentss) {
        for (Component[] components: componentss) {
            verticalLayout.addComponents(components);
        }
        return this;
    }

    public FluentVerticalLayout addComponentAttachListeners(HasComponents.ComponentAttachListener... componentAttachListeners) {
        for (HasComponents.ComponentAttachListener componentAttachListener: componentAttachListeners) {
            verticalLayout.addComponentAttachListener(componentAttachListener);
        }
        return this;
    }

    public FluentVerticalLayout componentError(final ErrorMessage arg0) {
        verticalLayout.setComponentError(arg0);
        return this;
    }

    public FluentVerticalLayout styleName(final String arg0) {
        verticalLayout.setStyleName(arg0);
        return this;
    }

    public FluentVerticalLayout captionAsHtml(final boolean arg0) {
        verticalLayout.setCaptionAsHtml(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #captionAsHtml(true)
     */
    public FluentVerticalLayout captionAsHtml() {
        return this.captionAsHtml(true);
    }

    public FluentVerticalLayout enabled(final boolean arg0) {
        verticalLayout.setEnabled(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #enabled(true)
     */
    public FluentVerticalLayout enabled() {
        return this.enabled(true);
    }

    public FluentVerticalLayout readOnly(final boolean arg0) {
        verticalLayout.setReadOnly(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #readOnly(true)
     */
    public FluentVerticalLayout readOnly() {
        return this.readOnly(true);
    }

    public FluentVerticalLayout spacing(final boolean arg0) {
        verticalLayout.setSpacing(arg0);
        return this;
    }

    /**
     * 
     * @return
     *     #spacing(true)
     */
    public FluentVerticalLayout spacing() {
        return this.spacing(true);
    }

    public FluentVerticalLayout addComponentAsFirsts(Component... componentAsFirsts) {
        for (Component componentAsFirst: componentAsFirsts) {
            verticalLayout.addComponentAsFirst(componentAsFirst);
        }
        return this;
    }

    public FluentVerticalLayout errorHandler(final ErrorHandler arg0) {
        verticalLayout.setErrorHandler(arg0);
        return this;
    }

    public FluentVerticalLayout componentAlignment(final Component arg0, final Alignment arg1) {
        verticalLayout.setComponentAlignment(arg0, arg1);
        return this;
    }

    public FluentVerticalLayout caption(final String arg0) {
        verticalLayout.setCaption(arg0);
        return this;
    }

    public FluentVerticalLayout primaryStyleName(final String arg0) {
        verticalLayout.setPrimaryStyleName(arg0);
        return this;
    }

    public FluentVerticalLayout sizeUndefined() {
        verticalLayout.setSizeUndefined();
        return this;
    }

    public FluentVerticalLayout height(final float arg0, final com.vaadin.server.Sizeable.Unit arg1) {
        verticalLayout.setHeight(arg0, arg1);
        return this;
    }

    public FluentVerticalLayout addStyleNames(String... styleNames) {
        for (String styleName: styleNames) {
            verticalLayout.addStyleName(styleName);
        }
        return this;
    }

    public FluentVerticalLayout data(final Object arg0) {
        verticalLayout.setData(arg0);
        return this;
    }

    public FluentVerticalLayout width(final String arg0) {
        verticalLayout.setWidth(arg0);
        return this;
    }

    public FluentVerticalLayout addListeners(Component.Listener... listeners) {
        for (Component.Listener listener: listeners) {
            verticalLayout.addListener(listener);
        }
        return this;
    }

    public FluentVerticalLayout addComponentDetachListeners(HasComponents.ComponentDetachListener... componentDetachListeners) {
        for (HasComponents.ComponentDetachListener componentDetachListener: componentDetachListeners) {
            verticalLayout.addComponentDetachListener(componentDetachListener);
        }
        return this;
    }

    public FluentVerticalLayout parent(final HasComponents arg0) {
        verticalLayout.setParent(arg0);
        return this;
    }

    public FluentVerticalLayout expandRatio(final Component arg0, final float arg1) {
        verticalLayout.setExpandRatio(arg0, arg1);
        return this;
    }

    public FluentVerticalLayout description(final String arg0) {
        verticalLayout.setDescription(arg0);
        return this;
    }

}
