package com.firetms.vanilla.ui.utils;

import org.springframework.util.StringUtils;

/**
 * Id generator for fields and button ids
 */
public class IdGen {

    public static String genId(String groupId, String id) {
        return groupId + StringUtils.capitalize(id);
    }
}
