package com.firetms.vanilla.ui.elements;

import com.vaadin.ui.TextField;
import com.firetms.vanilla.ui.utils.IdGen;

public class FTextField {

    private final TextField textField;

    private String id;

    public FTextField(String id) {
        this.textField = new TextField();
        this.textField.setId(id);
        this.nullRepresentation("");
    }

    public static FTextField textField(String id) {
        return new FTextField(id);
    }

    public static FTextField textField(String groupId, String id) {
        return new FTextField(IdGen.genId(groupId, id));
    }

    public FTextField caption(String caption) {
        this.textField.setCaption(caption);
        return this;
    }

    public FTextField nullRepresentation(final String value) {
        textField.setNullRepresentation(value);
        return this;
    }

    public TextField build() {
        return this.textField;
    }
}
