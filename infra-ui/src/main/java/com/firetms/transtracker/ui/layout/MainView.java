package com.firetms.transtracker.ui.layout;

import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vaadin.spring.sidebar.components.ValoSideBar;

/*
 * Dashboard MainView is a simple HorizontalLayout that wraps the menu on the
 * left and creates a simple container for the navigator on the right.
 */
@Component
@UIScope
public class MainView extends HorizontalLayout {

    private final ComponentContainer contentContainer;

    @Autowired
    public MainView(ValoSideBar sideBar) {
        setHeight(100, Unit.PERCENTAGE);
        addComponent(sideBar);

        contentContainer = new CssLayout();
        contentContainer.addStyleName("layout-contentContainer");
        contentContainer.setSizeFull();
        addComponent(contentContainer);
    }

    public ComponentContainer getContentContainer() {
        return contentContainer;
    }
}
