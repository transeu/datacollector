package com.firetms.transtracker.ui.vehicles;

import com.firetms.transtracker.domain.vehicles.Vehicle;
import com.firetms.transtracker.services.vehicles.VehicleManagementService;
import com.firetms.vanilla.ui.BaseCRUDForm;
import com.firetms.vanilla.ui.FUI;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.i18n.I18N;

import java.util.Locale;

@SpringComponent
@UIScope
public class VehicleEditor extends BaseCRUDForm<Vehicle> {

    @Autowired
    VehicleManagementService service;

    interface Id {
        String FORM = "vehicle";
        String VIN = "vin";
        String PLATE_NUMBER = "plateNumber";
        String NAME = "name";
    }

    /* Fields to edit properties in Vehicle entity */
    TextField vin;
    TextField name;
    TextField plateNumber;

    public VehicleEditor() {
        super(Id.FORM);
        addComponents(vin, plateNumber, name, actions);
    }

    @Override
    protected void initFields() {
        vin = FUI.textField(Id.FORM, Id.VIN)
                .build();
        name = FUI.textField(Id.FORM, Id.NAME)
                .build();
        plateNumber = FUI.textField(Id.FORM, Id.PLATE_NUMBER)
                .build();
    }

    @Override
    protected void wireButtons() {
        // wire action buttons to save, delete and reset
        actions.saveListener(e -> service.createVehicle(entity));
        actions.deleteListener(e -> service.deleteVehicle(vin.getValue()));
    }

    @Override
    protected void focusForEditing() {
        vin.selectAll();
    }

    @Override
    protected void updateMessageStrings( I18N i18N,  Locale locale ) {
        vin.setCaption( i18N.get( "vehicle.vin", locale ) );
        plateNumber.setCaption( i18N.get( "vehicle.plateNumber", locale ) );
        name.setCaption( i18N.get( "vehicle.name", locale ) );
    }

    @Override
    protected void instantiateEntity() {
        entity = new Vehicle();
    }
}