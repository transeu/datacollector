package com.firetms.transtracker.ui.vehicles;

import com.firetms.transtracker.domain.vehicles.Vehicle;
import com.firetms.transtracker.services.vehicles.VehicleManagementService;
import com.firetms.transtracker.ui.I18NProvider;
import com.firetms.transtracker.ui.layout.Sections;
import com.firetms.vanilla.ui.FilteredGrid;
import com.firetms.vanilla.ui.utils.FluentUI;
import com.firetms.vanilla.ui.utils.IdGen;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.elasticsearch.common.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.i18n.I18N;
import org.vaadin.spring.i18n.support.Translatable;
import org.vaadin.spring.sidebar.annotation.SideBarItem;

import java.util.Locale;

@SpringView(name = "")
@SideBarItem(sectionId = Sections.ADMINISTRATION, captionCode = "vehicles", order = 1)
public final class VehiclesView extends VerticalLayout implements View, Translatable {

    interface Id {
        String VIEW_NAME = "vehicles";
        String ADD = "add";
        String GRID = "grid";
    }

    private final Button addNewBtn;

    VehicleManagementService service;

    private final VehicleEditor editor;

    private FilteredGrid grid;


    @Autowired
    public VehiclesView( VehicleManagementService service, VehicleEditor editor) {
        this.service = service;
        this.editor = editor;

        setSizeFull();
        addStyleName("transactions");


        addNewBtn = FluentUI.button( FontAwesome.PLUS ).id(IdGen.genId(Id.VIEW_NAME, Id.ADD))
                            .addClickListeners( e -> editor.edit( new Vehicle( "" ) ) )
                            .get();

        grid = new FilteredGrid(IdGen.genId(Id.VIEW_NAME, Id.GRID), "vehicles.filter_prompt" )
                .translationPrefix( "vehicle" )
                .heightByRows( 10 )
                .addAction( addNewBtn )
                .dataSource( new BeanItemContainer<>( Vehicle.class ) )
                .columnOrder( "name", "plateNumber", "vin" )
                .addFilterChangeListener( e -> listEntities( e.getText() ) )
                .addFilterTriggerListener( this::listEntities )
                .addSelectionListener( ( Vehicle e ) -> {
                    // Connect selected driver to editor or hide if none is selected
                    if (e == null) {
                        editor.setVisible( false );
                    } else {
                        editor.edit( e );
                    }
                } );

        // build layout
        HorizontalLayout layout = FluentUI.horizontalLayout( grid, editor  ).margin().spacing().get();
        addComponent( layout );

        // Listen changes made by the editor, refresh data from backend
        editor.setChangeListener(() -> {
            editor.setVisible(false);
            listEntities( grid.getFilter() );
        });

        listEntities("");
    }

    private void listEntities(String query) {
        if (StringUtils.isEmpty(query)) {
            grid.dataSource( new BeanItemContainer<>(Vehicle.class, service.find()) );
        } else {
            grid.dataSource( new BeanItemContainer<>(Vehicle.class, service.find(query)) );
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        updateMessageStrings( getUI().getLocale() );
    }

    @Override
    public void updateMessageStrings( Locale locale ) {
        I18N i18N = ((I18NProvider) UI.getCurrent()).get();
        addNewBtn.setCaption( i18N.get( "vehicles.new", locale ) );
    }
}