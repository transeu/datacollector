package com.firetms.transtracker.ui.layout;

import org.springframework.stereotype.Component;
import org.vaadin.spring.sidebar.annotation.SideBarSection;
import org.vaadin.spring.sidebar.annotation.SideBarSections;

@SideBarSections({
        @SideBarSection(id = Sections.ADMINISTRATION, captionCode = "sections.administration"),
        @SideBarSection(id = Sections.DEMO, caption = "Demo")
})
@Component
public class Sections {
    public static final String ADMINISTRATION = "administration";
    public static final String DEMO = "demo";
}
