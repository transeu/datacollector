package com.firetms.transtracker.ui;

import com.firetms.transtracker.ui.layout.MainView;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.i18n.I18N;
import org.vaadin.spring.i18n.support.Translatable;
import org.vaadin.spring.i18n.support.TranslatableUI;

import java.util.Locale;

@SpringUI
@Theme("valo")
@Title("TransTracker")
public class TransTrackerUI extends TranslatableUI implements Translatable, I18NProvider {
    private static final Logger log = LoggerFactory.getLogger( TransTrackerUI.class );

    @Autowired
    private SpringViewProvider viewProvider;

    @Autowired
    private MainView mainView;

    @Autowired
    I18N i18n;

    private static final String DEFAULT_LANG = Lang.EN;
    interface Lang {
        String EN = "en";
        String PL = "pl";
    }
    private Locale pl = new Locale( Lang.PL );
    private NativeSelect lang;

    @Override
    protected void initUI( VaadinRequest request ) {
        Responsive.makeResponsive(this);

        setSizeFull();
        addStyleName("mainview");

        Navigator navigator = new Navigator(this, mainView.getContentContainer());
        navigator.addProvider(viewProvider);
        navigator.addViewChangeListener( new ViewChangeListener() {
            @Override
            public boolean beforeViewChange( ViewChangeEvent event ) {
                return true;
            }

            @Override
            public void afterViewChange( ViewChangeEvent event ) {
                updateMessageStrings();
            }
        } );

        setContent( buildView() );

        setLanguage( request.getLocale() );
    }

    private void setLanguage( Locale locale ) {
        String language = locale.toString();
        if(!(Lang.EN.equals( language ) || Lang.PL.equals( language )))
            language = DEFAULT_LANG;
        lang.select( language );
    }

    private HorizontalLayout buildView() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setSizeFull();
//        layout.setWidth( 100, Unit.PERCENTAGE );
        lang = new NativeSelect();
        lang.addItems( Lang.EN, Lang.PL );
        lang.setNullSelectionAllowed( false );
        lang.addValueChangeListener( event -> toggleLanguage( (String) lang.getValue() ) );
        lang.setImmediate( true );
        //lang.setWidth( 100, Unit.PERCENTAGE );
        /*FormLayout fl = new FormLayout();
        fl.addComponent( lang );*/
        layout.addComponents( mainView, lang/*fl*/ );
        layout.setExpandRatio( mainView, 1.0f );
        return layout;
    }

    private void toggleLanguage( String value ) {
        if (Lang.PL.equals( value ) )
            setLocale( pl );
        else
            setLocale( Locale.ENGLISH );
    }

    @Override
    public void updateMessageStrings( Locale locale ) {
        //log.info( "updateMessageStrings({})", locale.toLanguageTag() );
        String caption = i18n.get( "menu.language", locale );
        //log.info( "button caption: {}", caption );
//        lang.setCaption( caption );
    }

    @Override
    public I18N get() {
        return i18n;
    }
}
