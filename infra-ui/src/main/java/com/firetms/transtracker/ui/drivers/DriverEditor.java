package com.firetms.transtracker.ui.drivers;

import com.firetms.transtracker.domain.drivers.Driver;
import com.firetms.transtracker.services.drivers.DriverManagementService;
import com.firetms.vanilla.ui.BaseCRUDForm;
import com.firetms.vanilla.ui.FUI;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.i18n.I18N;

import java.util.Locale;

@SpringComponent
@UIScope
public class DriverEditor extends BaseCRUDForm<Driver> {
    public interface Id {
        String FORM = "driver";
        String NAME = "name";
        String LAST_NAME = "last_name";
        String PHONE = "phone";
        String LICENSE = "license";
    }

    @Autowired
    DriverManagementService service;

    private TextField name;
    private TextField surname;
    private TextField phoneNumber;
    private TextField licenseNumber;

    public DriverEditor() {
        super(Id.FORM);
        addComponents(name, surname, phoneNumber, licenseNumber, actions);
    }

    @Override
    protected void initFields() {
        name = FUI.textField(Id.FORM, Id.NAME)
                .caption("First name")
                .build();
        surname = FUI.textField(Id.FORM, Id.LAST_NAME)
                .caption("Last name")
                .build();
        phoneNumber = FUI.textField(Id.FORM, Id.PHONE)
                .caption("Phone")
                .build();
        licenseNumber = FUI.textField(Id.FORM, Id.LICENSE)
                .caption("license number")
                .build();
    }

/*
    @Override
    public void updateMessageStrings( Locale locale ) {
//        name.setCaption( i18n );
    }
*/

    @Override
    protected void focusForEditing() {
        //focus first field
        name.selectAll();
    }

    @Override
    protected void updateMessageStrings( I18N i18N,  Locale locale ) {
        name.setCaption( i18N.get( "driver.name", locale ) );
        surname.setCaption( i18N.get( "driver.surname", locale ) );
        phoneNumber.setCaption( i18N.get( "driver.phoneNumber", locale ) );
        licenseNumber.setCaption( i18N.get( "driver.licenseNumber", locale ) );
    }

    @Override
    protected void wireButtons() {
        actions.saveListener(e -> service.create(entity));
        actions.deleteListener(e -> service.delete(entity.getId()));
        //cancel.addClickListener(e -> editVehicle(vehicle));
    }

    @Override
    protected void instantiateEntity() {
        entity = new Driver();
    }
}
