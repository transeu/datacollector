package com.firetms.transtracker.ui;

import org.vaadin.spring.i18n.I18N;

/**
 * Created by olek on 11.07.2016.
 */
public interface I18NProvider {
    I18N get();
}
