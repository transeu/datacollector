package com.firetms.transtracker.ui.drivers;

import com.firetms.transtracker.domain.drivers.Driver;
import com.firetms.transtracker.services.drivers.DriverManagementService;
import com.firetms.transtracker.ui.I18NProvider;
import com.firetms.transtracker.ui.layout.Sections;
import com.firetms.vanilla.ui.FilteredGrid;
import com.firetms.vanilla.ui.utils.FluentUI;
import com.firetms.vanilla.ui.utils.IdGen;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.vaadin.spring.i18n.I18N;
import org.vaadin.spring.i18n.support.Translatable;
import org.vaadin.spring.sidebar.annotation.SideBarItem;

import java.util.Locale;

@SpringView(name = DriversView.Id.VIEW_NAME)
@SideBarItem(sectionId = Sections.ADMINISTRATION, captionCode = "drivers", order = 1)
public class DriversView extends VerticalLayout implements View, Translatable {
    private static final Logger log = LoggerFactory.getLogger(DriversView.class);

    interface Id {
        String VIEW_NAME = "drivers";
        String ADD = "add";
        String GRID = "grid";
    }

    private final Button addNewBtn;
    DriverManagementService service;

    private final DriverEditor editor;

    private FilteredGrid grid;

    @Autowired
    public DriversView(DriverManagementService service, DriverEditor editor) {
        this.service = service;
        this.editor = editor;

        addNewBtn = FluentUI.button(FontAwesome.PLUS).id(IdGen.genId(Id.VIEW_NAME, Id.ADD))
                .addClickListeners(e -> editor.edit(new Driver("")))
                .get();

        grid = new FilteredGrid(IdGen.genId(Id.VIEW_NAME, Id.GRID), "drivers.filter_prompt")
                .translationPrefix("driver")
                .heightByRows(10)
                .addAction(addNewBtn)
                .dataSource(new BeanItemContainer<>(Driver.class))
                .columnOrder("surname", "name", "phoneNumber", "licenseNumber")
                .removeColumn("id")
                .addFilterChangeListener(e -> listEntities(e.getText()))
                .addFilterTriggerListener(this::listEntities)
                .addSelectionListener((Driver e) -> {
                    // Connect selected driver to editor or hide if none is selected
                    if (e == null) {
                        editor.setVisible(false);
                    } else {
                        editor.edit(e);
                    }
                });

        // build layout
        HorizontalLayout layout = FluentUI.horizontalLayout(grid, editor).margin().spacing().get();
        addComponent(layout);

        // Listen changes made by the editor, refresh data from backend
        editor.setChangeListener(() -> {
            editor.setVisible(false);
            listEntities(grid.getFilter());
        });

        listEntities("");
    }

    private void listEntities(String query) {
        log.trace("listEntities({})", query);
        if (StringUtils.isEmpty(query)) {
            grid.dataSource( new BeanItemContainer<>(Driver.class, service.find()) );
        } else {
            grid.dataSource( new BeanItemContainer<>(Driver.class, service.find(query)) );
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        updateMessageStrings(getUI().getLocale());
    }

    @Override
    public void updateMessageStrings(Locale locale) {
        I18N i18N = ((I18NProvider) UI.getCurrent()).get();
        addNewBtn.setCaption(i18N.get("drivers.new_driver", locale));
    }
}
