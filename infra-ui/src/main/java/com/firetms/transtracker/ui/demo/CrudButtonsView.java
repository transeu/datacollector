package com.firetms.transtracker.ui.demo;

import com.firetms.transtracker.ui.layout.Sections;
import com.firetms.vanilla.ui.FUI;
import com.firetms.vanilla.ui.components.CRUDButtons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.spring.sidebar.annotation.SideBarItem;

@SpringView(name = CrudButtonsView.VIEW_NAME)
@SideBarItem(sectionId = Sections.DEMO, caption = "CRUDButtons", order = 1)
public class CrudButtonsView extends VerticalLayout implements View{

    public static final String VIEW_NAME = "crudButtons";

    private final Button addNewBtn;

    private CRUDButtons buttons;

    public CrudButtonsView() {
        buttons = FUI.crudButtons(VIEW_NAME);

        addNewBtn = new Button("New vehicle",FontAwesome.PLUS);
        addComponents(buttons);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

    }
}
