package com.firetms.transtracker.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.firetms.transtracker.domain.drivers.DriverRepository;
import com.firetms.transtracker.domain.vehicles.VehicleRepository;
import com.firetms.transtracker.infra.elasticsearch.ElasticsearchDriverRepository;
import com.firetms.transtracker.infra.elasticsearch.ElasticsearchMetricRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.EntityMapper;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import java.io.IOException;

import static org.elasticsearch.node.NodeBuilder.nodeBuilder;

@Configuration
@EnableElasticsearchRepositories(basePackageClasses = {VehicleRepository.class, ElasticsearchMetricRepository.class,
        DriverRepository.class})
@ConditionalOnProperty(name = "elasticsearch.enabled", havingValue = "true")
public class ElasticsearchConfig {

    @Autowired
    ObjectMapper objectMapper;

//    @Bean
//    public Client client() {
//        TransportClient client = new TransportClient();
//        //TransportAddress address = new InetSocketTransportAddress(environment.getProperty("elasticsearch.host"), Integer.parseInt(environment.getProperty("elasticsearch.port")));
//        try {
//            TransportAddress address = new InetSocketTransportAddress(InetAddress.getByName("logs.dev.alan-systems.local"), 9300);
//            client.addTransportAddress(address);
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//        }
//
//        return client;
//    }
//
//    @Bean
//    public ElasticsearchOperations elasticsearchTemplate() {
//        return new ElasticsearchTemplate(client());
//    }

    @Bean
    public ElasticsearchOperations elasticsearchTemplate() {
        return new ElasticsearchTemplate(nodeBuilder().local(true).node().client(), new ConfiguredEntityMapper());
    }

    public class ConfiguredEntityMapper implements EntityMapper {


        public ConfiguredEntityMapper() {
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        }

        @Override
        public String mapToString(Object object) throws IOException {
            return objectMapper.writeValueAsString(object);
        }

        @Override
        public <T> T mapToObject(String source, Class<T> clazz) throws IOException {
            return objectMapper.readValue(source, clazz);
        }
    }

}
