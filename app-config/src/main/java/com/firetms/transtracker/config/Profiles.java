package com.firetms.transtracker.config;

public class Profiles {
    public static final String DEFAULT = "default";
    public static final String PROD = "prod";
    public static final String DEV = "dev";
    public static final String INTEGRATION_TESTS = "integration-tests";
    public static final String UNIT_TESTS = "unit-tests";
}
