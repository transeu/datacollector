package com.firetms.transtracker.infra.mock;

import com.firetms.transtracker.domain.metrics.Metric;
import com.firetms.transtracker.domain.metrics.MetricRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
@ConditionalOnProperty(name = "mocks.enabled", havingValue = "true")
public class MetricRepositoryMock implements MetricRepository {

    @Override
    public void storeMetric(Metric metric) {
        System.out.println(metric.toString());
    }

    @Override
    public List<Metric> readMetrics(String vin) {
        return Arrays.asList(LocationMetricFixture.rybnik(), LocationMetricFixture.rybnik());
    }
}
