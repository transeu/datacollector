package com.firetms.transtracker.infra.mock;

import com.firetms.transtracker.domain.metrics.LocationMetric;

import java.time.LocalDateTime;

public class LocationMetricFixture {

    public static final LocalDateTime TIMESTAMP = LocalDateTime.of(2016, 06, 14, 16, 26, 31);
    public static final double LATITUDE = 50.098611;
    public static final double LONGITUDE = 18.545;

    public static LocationMetric rybnik() {
        return LocationMetric.from(
                VinFixture.VIN,
                TIMESTAMP,
                LATITUDE,
                LONGITUDE);
    }
}
