package com.firetms.transtracker.infra.mock;

import com.firetms.transtracker.domain.vehicles.Vehicle;

public class VehicleFixture {

    public static final Vehicle vehicle() {
        Vehicle vehicle = new Vehicle(VinFixture.VIN);
        vehicle.setPlateNumber("SR12345");
        return vehicle;
    }
}
