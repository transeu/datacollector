package com.firetms.transtracker.infra.mock;

import com.firetms.transtracker.domain.drivers.Driver;

public class DriverFixture {

    public static Driver entity() {
        return Driver.aDriver().id("123").name( "Jan" ).surname( "Kowalski" ).build();
    }
}
