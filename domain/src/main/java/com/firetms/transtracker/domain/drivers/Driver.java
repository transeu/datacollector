package com.firetms.transtracker.domain.drivers;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "drivers")
public class Driver {
    @Id
    private String id;

    private String name;
    private String surname;
    private String phoneNumber;
    private String licenseNumber;

    public Driver() {
    }

    public Driver( String id ) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId( String id ) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname( String surname ) {
        this.surname = surname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber( String phoneNumber ) {
        this.phoneNumber = phoneNumber;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber( String licenseNumber ) {
        this.licenseNumber = licenseNumber;
    }

    @Override
    public boolean equals( Object o ) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Driver driver = (Driver) o;

        if (id != null ? !id.equals( driver.id ) : driver.id != null) return false;
        if (name != null ? !name.equals( driver.name ) : driver.name != null) return false;
        if (surname != null ? !surname.equals( driver.surname ) : driver.surname != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals( driver.phoneNumber ) : driver.phoneNumber != null) return false;
        return licenseNumber != null ? licenseNumber.equals( driver.licenseNumber ) : driver.licenseNumber == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (licenseNumber != null ? licenseNumber.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", licenseNumber='" + licenseNumber + '\'' +
                '}';
    }

    public static Builder aDriver() {
        return new Builder();
    }

    public static final class Builder {
        private String id;
        private String name;
        private String surname;
        private String phoneNumber;
        private String licenseNumber;

        private Builder() {
        }

        public Builder id( String id ) {
            this.id = id;
            return this;
        }

        public Builder name( String name ) {
            this.name = name;
            return this;
        }

        public Builder surname( String surname ) {
            this.surname = surname;
            return this;
        }

        public Builder phoneNumber( String phoneNumber ) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public Builder licenseNumber( String licenseNumber ) {
            this.licenseNumber = licenseNumber;
            return this;
        }

        public Driver build() {
            Driver driver = new Driver();
            driver.setId( id );
            driver.setName( name );
            driver.setSurname( surname );
            driver.setPhoneNumber( phoneNumber );
            driver.setLicenseNumber( licenseNumber );
            return driver;
        }
    }
}
