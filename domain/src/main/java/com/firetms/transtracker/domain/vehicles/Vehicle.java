package com.firetms.transtracker.domain.vehicles;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.util.StringUtils;

import java.util.Objects;

@Document(indexName = "vehicles")
public class Vehicle {

    @Id
    private String vin;

    private String plateNumber;

    private String name;

    public Vehicle() {}

    public Vehicle(String vin) {
        this.vin = vin;
    }

    public String getVin() {
        return vin;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return Objects.equals(vin, vehicle.vin) &&
                Objects.equals(plateNumber, vehicle.plateNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vin, plateNumber);
    }


    public static final class Builder {
        private String name;
        private String vin;
        private String plateNumber;

        private Builder() {
        }

        public static Builder aVehicle() {
            return new Builder();
        }

        public Builder withName(String name) {
            if (StringUtils.hasLength(name)) this.name = name;
            return this;
        }

        public Builder withVin(String vin) {
            this.vin = vin;
            return this;
        }

        public Builder withPlateNumber(String plateNumber) {
            if (StringUtils.hasLength(plateNumber)) this.plateNumber = plateNumber;
            return this;
        }

        public Vehicle build() {
            Vehicle vehicle = new Vehicle();
            vehicle.setName(name);
            vehicle.setVin(vin);
            vehicle.setPlateNumber(plateNumber);
            return vehicle;
        }
    }
}
