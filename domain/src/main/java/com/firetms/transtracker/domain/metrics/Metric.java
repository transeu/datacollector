package com.firetms.transtracker.domain.metrics;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public abstract class Metric {

    String id;

    String vin;

    Long timestamp;

    public abstract String getMetricType();

    public String getId() {
        return id;
    }

    public String getVin() {
        return vin;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public LocalDateTime timestampAsLocalDateTime() {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(timestamp), ZoneId.of("UTC"));
    }
}
