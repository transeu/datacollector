package com.firetms.transtracker.domain.metrics;

import java.util.List;

public interface MetricRepository {
    void storeMetric(Metric metric);
    List<Metric> readMetrics(String vin);
}
