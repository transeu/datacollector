package com.firetms.transtracker.domain.vehicles;

import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Iterator;
import java.util.Spliterators;

public interface VehicleRepository extends CrudRepository<Vehicle, String> {
    Iterable<Vehicle> findByPlateNumberStartsWithIgnoreCase(String query);
}
