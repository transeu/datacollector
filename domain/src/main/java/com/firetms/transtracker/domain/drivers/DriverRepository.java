package com.firetms.transtracker.domain.drivers;

import org.springframework.data.repository.CrudRepository;

public interface DriverRepository extends CrudRepository<Driver, String> {
    Iterable<Driver> findBySurnameStartsWithIgnoreCase(String query);
}
