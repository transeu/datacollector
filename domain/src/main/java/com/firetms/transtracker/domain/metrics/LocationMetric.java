package com.firetms.transtracker.domain.metrics;

import org.springframework.data.elasticsearch.annotations.Document;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Objects;

@Document(indexName = "metrics", type = "LocationMetric")
public class LocationMetric extends Metric {

    private GeoLocation geoLocation;

    public static final String METRIC_TYPE = "GLL";

    //Only for ORMs
    private LocationMetric() {}

    private LocationMetric(String vin, LocalDateTime timestamp, double theLatitude, double theLongitude) {
        this.timestamp = timestamp.toEpochSecond(ZoneOffset.UTC);
        this.vin = vin;
        this.geoLocation = new GeoLocation(theLatitude, theLongitude);
        StringBuilder sb = new StringBuilder();
        sb.append(this.vin);
        sb.append(METRIC_TYPE);
        sb.append(this.timestamp);
        this.id = sb.toString();
    }

    public static LocationMetric from(String vin, LocalDateTime timestamp, double theLatitude, double theLongitude) {
        return new LocationMetric(vin, timestamp, theLatitude, theLongitude);
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    @Override
    public String getMetricType() {
        return METRIC_TYPE;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LocationMetric{");
        sb.append("id=").append(getId()).append(", ");
        sb.append("timestamp=").append(timestampAsLocalDateTime()).append(", ");
        sb.append("geoLocation=").append(geoLocation);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LocationMetric)) return false;
        LocationMetric that = (LocationMetric) o;
        return Objects.equals(getGeoLocation(), that.getGeoLocation());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getGeoLocation());
    }
}
