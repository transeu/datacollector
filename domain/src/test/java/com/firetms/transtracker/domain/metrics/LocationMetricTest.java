package com.firetms.transtracker.domain.metrics;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class LocationMetricTest {

    public static final String VIN = "AXN1923423424234";
    public static final LocalDateTime TIMESTAMP = LocalDateTime.of(2016, 06, 30, 10, 10, 11);
    public static final double LATITUDE = 50.11;
    public static final double LONGITUDE = 18.10;

    @Test
    public void shouldGenerateId() throws Exception {
        LocationMetric locationMetric = LocationMetric.from(
                VIN,
                TIMESTAMP,
                LATITUDE, LONGITUDE);
        assertThat(locationMetric.getVin(), is(VIN));
        assertThat(locationMetric.timestampAsLocalDateTime(), is(TIMESTAMP));
        assertThat(locationMetric.getGeoLocation(), is(GeoLocation.of(LATITUDE, LONGITUDE)));
        assertThat(locationMetric.getId(),
                is(VIN + LocationMetric.METRIC_TYPE + TIMESTAMP.toEpochSecond(ZoneOffset.UTC)));
    }
}