package com.firetms.transtracker.infra.elasticsearch;

import com.firetms.transtracker.domain.drivers.Driver;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

@ConditionalOnProperty(name = "elasticsearch.enabled", havingValue = "true")
public interface ElasticsearchDriverRepository extends ElasticsearchCrudRepository<Driver, String> {
//    List<Metric> findByVin( String vin );
}
