package com.firetms.transtracker.infra.elasticsearch;

import com.firetms.transtracker.domain.metrics.LocationMetric;
import com.firetms.transtracker.domain.metrics.Metric;
import com.firetms.transtracker.domain.metrics.MetricRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ConditionalOnProperty(name = "elasticsearch.enabled", havingValue = "true")
public class ElasticsearchMetricRepositoryAdapter implements MetricRepository {

    @Autowired
    ElasticsearchMetricRepository elasticsearchMetricRepository;

    @Override
    public void storeMetric(Metric metric) {
        if (metric instanceof LocationMetric) {
            LocationMetric locationMetric = (LocationMetric) metric;
            elasticsearchMetricRepository.save(locationMetric);
        }
    }

    @Override
    public List<Metric> readMetrics(String vin) {
        return elasticsearchMetricRepository.findByVin(vin);
    }
}