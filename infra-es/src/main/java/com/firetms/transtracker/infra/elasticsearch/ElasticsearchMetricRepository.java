package com.firetms.transtracker.infra.elasticsearch;

import com.firetms.transtracker.domain.metrics.LocationMetric;
import com.firetms.transtracker.domain.metrics.Metric;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import java.util.List;

@ConditionalOnProperty(name = "elasticsearch.enabled", havingValue = "true")
public interface ElasticsearchMetricRepository extends ElasticsearchCrudRepository<LocationMetric, String> {

    List<Metric> findByVin(String vin);
}
