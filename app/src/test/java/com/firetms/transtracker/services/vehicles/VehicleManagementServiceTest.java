package com.firetms.transtracker.services.vehicles;

import com.firetms.transtracker.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.*;

@SpringApplicationConfiguration(Application.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class VehicleManagementServiceTest {

    @Autowired
    VehicleManagementService service;

    @Test
    public void shouldWireService() throws Exception {
        assertThat(service, is(not(nullValue())));
    }


}