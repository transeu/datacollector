package com.firetms.transtracker.services.drivers;

import com.firetms.transtracker.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

@SpringApplicationConfiguration(Application.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class DriverManagementServiceTest {

    @Autowired
    DriverManagementService service;

    @Test
    public void shouldWireService() throws Exception {
        assertThat(service, is(not(nullValue())));
    }


}