package com.firetms.transtracker.config;

import com.firetms.transtracker.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.hasItemInArray;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@SpringApplicationConfiguration(Application.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(Profiles.DEFAULT)
public class DefaultProfileConfigTest {

    @Autowired
    Environment environment;

    @Test
    public void shouldHaveDefaultPropertiesSet() throws Exception {
        assertThat(environment.getDefaultProfiles(), hasItemInArray(Profiles.DEFAULT));
        assertThat(environment.getProperty("elasticsearch.enabled"), is("true"));
        assertThat(environment.getProperty("mocks.enabled"), is(nullValue()));
    }
}
