package com.firetms.transtracker.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.firetms.transtracker.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.*;

@SpringApplicationConfiguration(Application.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class JacksonConfigTest {

    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void shouldCreateObjectMapper() throws Exception {
        assertThat(objectMapper, is(not(nullValue())));

    }

    @Test
    public void shouldRegisterJavaTimeModule() throws Exception {
        //assertThat(objectMapper.canDeserialize(LocalDateTime), is(true));
        //assertThat(objectMapper., is(true));

    }
}