package com.firetms.transtracker.config;

import com.firetms.transtracker.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.*;

@SpringApplicationConfiguration(Application.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(Profiles.DEFAULT)
public class ElasticsearchConfigTest {

    @Autowired
    ElasticsearchOperations elasticsearchOperations;

    @Test
    public void shouldWireElasticsearchOperations() throws Exception {
        assertThat(elasticsearchOperations, is(not(nullValue())));
    }
}