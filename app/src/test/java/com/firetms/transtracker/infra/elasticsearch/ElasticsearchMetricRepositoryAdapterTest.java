package com.firetms.transtracker.infra.elasticsearch;

import com.firetms.transtracker.Application;
import com.firetms.transtracker.config.Profiles;
import com.firetms.transtracker.domain.metrics.LocationMetric;
import com.firetms.transtracker.infra.mock.LocationMetricFixture;
import com.firetms.transtracker.domain.metrics.Metric;
import com.firetms.transtracker.infra.mock.VinFixture;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

@SpringApplicationConfiguration(Application.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(Profiles.INTEGRATION_TESTS)
public class ElasticsearchMetricRepositoryAdapterTest {

    @Autowired
    ElasticsearchMetricRepositoryAdapter repo;

    @Test
    public void shouldWireRepo() throws Exception {
        assertThat(repo, is(not(nullValue())));
    }

    @Test
    public void shouldStoreMetric() throws Exception {
        LocationMetric location = LocationMetricFixture.rybnik();
        String locationMetricId = location.getId();
        repo.storeMetric(location);
        System.out.println(location);
        List<Metric> savedLocations = repo.readMetrics(VinFixture.VIN);
        System.out.println(savedLocations);
        assertThat(savedLocations.size(), is(1));
        assertThat(savedLocations.get(0), is(location));
    }
}