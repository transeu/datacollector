package com.firetms.transtracker.infra.rest;

import com.firetms.transtracker.Application;
import com.firetms.transtracker.config.Profiles;
import com.firetms.transtracker.domain.metrics.LocationMetric;
import com.firetms.transtracker.domain.metrics.Metric;
import com.firetms.transtracker.domain.metrics.MetricRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

@SpringApplicationConfiguration(Application.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(Profiles.UNIT_TESTS)
public class MetricsRestControllerTest {

    @Autowired
    MetricsRestController metricsController;

    @Before
    public void setUp() throws Exception {
        //metricsController = new MetricsRestController();
        //metricsController.metricRepository = new MetricsService();
    }

    @Test
    public void shouldCreateMetricsController() throws Exception {
        assertThat(metricsController, is(not(nullValue())));
        assertThat(metricsController.metricRepository, is(not(nullValue())));
    }

    @Test
    public void shouldAcceptGeoLocationMetric() throws Exception {
        metricsController.createMetric("$GPGLL,4916.45,N,12311.12,W,225444,A*31");
    }

    @Test
    public void shouldAcceptListOfGeoLocationMetrics() throws Exception {
        metricsController.createMetric("$GPGLL,4916.45,N,12311.12,W,225444,A*31$GPGLL,4916.45,N,12311.12,W,225444,A*31");
    }

    @Test
    public void shouldConvertToLocationMetric() throws Exception {
        Metric metric = metricsController.parseMetric("$GPGLL,4916.45,N,12311.12,W,225444,A*31");
        assertThat(metric, instanceOf(LocationMetric.class));
        assertThat(metric.timestampAsLocalDateTime().getHour(), is(22));
        assertThat(metric.timestampAsLocalDateTime().getMinute(), is(54));
        assertThat(metric.timestampAsLocalDateTime().getSecond(), is(44));
        assertThat(metric.timestampAsLocalDateTime().getNano(), is(0));

    }

    @Test
    public void shouldSplitSentencesByDollar() throws Exception {
        List<String> result = metricsController.splitSentences("$GPGLL,4916.45,N,12311.12,W,225444,A*31$GPGLL,4916.45,N,12311.12,W,225444,A*31");
        assertThat(result, hasSize(2));
        assertThat(result.get(0), startsWith("$"));
    }
}