package com.firetms.transtracker.infra.elasticsearch;

import com.firetms.transtracker.Application;
import com.firetms.transtracker.config.Profiles;
import com.firetms.transtracker.domain.metrics.LocationMetric;
import com.firetms.transtracker.domain.metrics.Metric;
import com.firetms.transtracker.infra.mock.LocationMetricFixture;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.*;

@SpringApplicationConfiguration(Application.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ElasticsearchMetricRepositoryTest {

    @Autowired
    ElasticsearchOperations elasticsearchTemplate;

    @Autowired
    ElasticsearchMetricRepository repo;

    @Test
    public void shouldWireRepo() throws Exception {
        assertThat(repo, is(not(nullValue())));
    }

    @Test
    public void shouldStoreMetric() throws Exception {
        elasticsearchTemplate.deleteIndex(LocationMetric.class);
        LocationMetric location = LocationMetricFixture.rybnik();
        String locationMetricId = location.getId();
        repo.save(location);
        System.out.println(location);
        Metric savedLocation = repo.findOne(locationMetricId);
        System.out.println(savedLocation);

        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(matchAllQuery())
                .withIndices("metrics")
                .build();
        Page<LocationMetric> metrics = elasticsearchTemplate.queryForPage(searchQuery, LocationMetric.class);
        System.out.println(metrics);
        assertThat(metrics.getTotalElements(), is(1L));
        assertThat(metrics.getContent().get(0), is(location));
    }
}