package com.firetms.transtracker.domain.drivers;

import com.firetms.transtracker.Application;
import com.firetms.transtracker.infra.mock.DriverFixture;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;

@SpringApplicationConfiguration(Application.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class DriversRepositoryTest {

    @Autowired
    DriverRepository repo;

    @After
    public void tearDown() {
        repo.deleteAll();
    }

    @Test
    public void shouldCreateRepo() {
        assertThat(repo ).isNotNull();
    }

    @Test
    public void shouldManageDrivers() {
        long initialCount = repo.count();

        Driver entity = DriverFixture.entity();
        repo.save(entity);

        Driver readEntity = repo.findOne(entity.getId());
        assertThat( readEntity ).isEqualTo( entity );

        readEntity.setSurname("Anonim");
        repo.save(readEntity);

        Driver updatedEntity = repo.findOne(entity.getId());
        assertThat(updatedEntity).isEqualTo(readEntity);
        assertThat(updatedEntity).isNotEqualTo(entity);

        repo.delete(entity);
        assertThat(repo.count()).isEqualTo(initialCount);
    }

    @Test
    public void shouldAssignIdForNewEntity() {
        Driver entity = Driver.aDriver().name( "Gal" ).surname( "Anonim" ).build();
        Driver saved = repo.save( entity );

        assertThat( saved.getId() ).isNotEmpty();
    }

    @Test
    public void shouldStoreMoreDrivers() {
        long initialCount = repo.count();

        Driver entity = DriverFixture.entity();
        repo.save(entity);

        Driver second = Driver.aDriver().name( "Gal" ).surname( "Anonim" ).build();
        repo.save( second );

        assertThat(repo.count()).isEqualTo(initialCount + 2);
    }
}