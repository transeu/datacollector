package com.firetms.transtracker.domain.vehicles;

import com.firetms.transtracker.Application;
import com.firetms.transtracker.infra.mock.VehicleFixture;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

@SpringApplicationConfiguration(Application.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class VehicleRepositoryTest {

    @Autowired
    VehicleRepository repo;

    @Test
    public void shouldCreateRepo() throws Exception {
        assertThat(repo, is(not(nullValue())));

    }

    @Test
    public void shouldManageVehicle() throws Exception {
        long initialCount = repo.count();

        Vehicle vehicle = VehicleFixture.vehicle();
        repo.save(vehicle);

        Vehicle readVehicle = repo.findOne(vehicle.getVin());
        assertThat(readVehicle, is(vehicle));

        readVehicle.setPlateNumber("SGL12345");
        repo.save(readVehicle);
        Vehicle updatedVehicle = repo.findOne(vehicle.getVin());
        assertThat(updatedVehicle, is(readVehicle));
        assertThat(updatedVehicle, is(not(vehicle)));

        repo.delete(updatedVehicle);
        assertThat(repo.count(), is(initialCount));
    }

}