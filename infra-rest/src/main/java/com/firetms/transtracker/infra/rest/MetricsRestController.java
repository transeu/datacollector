package com.firetms.transtracker.infra.rest;

import com.firetms.transtracker.domain.metrics.LocationMetric;
import com.firetms.transtracker.domain.metrics.Metric;
import com.firetms.transtracker.domain.metrics.MetricRepository;
import net.sf.marineapi.nmea.parser.SentenceFactory;
import net.sf.marineapi.nmea.sentence.GLLSentence;
import net.sf.marineapi.nmea.sentence.Sentence;
import net.sf.marineapi.nmea.util.Time;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class MetricsRestController {

    private static final Logger log = LoggerFactory.getLogger(MetricsRestController.class);

    private final SentenceFactory sentenceFactory = SentenceFactory.getInstance();

    @Autowired
    MetricRepository metricRepository;

    @RequestMapping(value = "/metrics", method = RequestMethod.POST)
    public void createMetric(@RequestBody String encodedMetric) {
        Assert.notNull(encodedMetric, "Encoded encodedMetrics not provided");
        log.trace("Encoded metric: ", encodedMetric);
        List<String> encodedMetrics = splitSentences(encodedMetric);
        encodedMetrics.parallelStream().forEach(m -> {
            Metric metric = parseMetric(m);
            metricRepository.storeMetric(metric);
        });
    }

    @RequestMapping(value = "/metrics", method = RequestMethod.GET)
    public List<Metric> listMetrics(String vin) {
        return metricRepository.readMetrics(vin);
    }


    List<String> splitSentences(String encodedMetrics) {
        return new ArrayList<>(Arrays.asList(
                encodedMetrics
                        .replaceAll("\\s+", "")
                        .split("(?=\\$)")));
    }

    Metric parseMetric(String metric) {
        Sentence sentence = sentenceFactory.createParser(metric);
        LocationMetric locationMetric = null;
        if ("GLL".equals(sentence.getSentenceId())) {
            GLLSentence gllSentence = (GLLSentence) sentence;
            locationMetric = LocationMetric.from(
                    "DUMMYVIN123",
                    convert(gllSentence.getTime()),
                    gllSentence.getPosition().getLatitude(),
                    gllSentence.getPosition().getLongitude());
        }
        return locationMetric;
    }

    LocalDateTime convert(Time time) {
        return LocalDateTime.of(
                LocalDate.now(ZoneId.of("UTC")),
                LocalTime.of(
                    time.getHour(),
                    time.getMinutes(), new Double(time.getSeconds()).intValue()
                ));
    }

}